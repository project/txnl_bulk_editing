<?php

namespace Drupal\txnl_bulk_editing\ParamConverter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for upcasting entity type IDs to full entity types.
 *
 * In order to use it you should specify some additional options in your route:
 * @code
 * example.route:
 *   path: foo/{entity_type}
 *   options:
 *     parameters:
 *       entity_type:
 *         type: entity_type
 * @endcode
 */
class EntityTypeConverter implements ParamConverterInterface {

  /**
   * Entity type manager which performs the upcasting in the end.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor for EntityTypeConverter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
    /** @noinspection PhpUnhandledExceptionInspection */
    return $this->entityTypeManager->getDefinition($value, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    $type = $definition['type'] ?? NULL;

    return $type === 'entity_type';
  }

}
