<?php

namespace Drupal\txnl_bulk_editing\ParamConverter;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\DynamicEntityTypeParamConverterTrait;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for loading an entity by its UUID.
 *
 * In order to use it you should specify some additional options in your route:
 * @code
 * example.route:
 *   path: foo/{entity}
 *   options:
 *     parameters:
 *       entity:
 *         type: entity_uuid:<ENTITY TYPE>
 * @endcode
 */
class EntityUuidConverter implements ParamConverterInterface {

  use DynamicEntityTypeParamConverterTrait;

  /**
   * Entity type manager that is used to validate the entity type exists.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Repository that is used to load the entity by its UUID.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * Constructs a new EntityUuidConverter.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              EntityRepositoryInterface $entity_repository) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository  = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    try {
      $entity_type_id =
        $this->getEntityTypeFromDefaults($definition, $name, $defaults);

      $entity =
        $this->entityRepository->loadEntityByUuid($entity_type_id, $value);
    }
    catch (ParamNotConvertedException | EntityStorageException $e) {
      $entity = NULL;
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    $converter_type = $definition['type'] ?? '';

    if (preg_match('/^entity_uuid:(.+)$/', $converter_type, $matches)) {
      $entity_type_id = $matches[1];

      return $this->entityTypeManager->hasDefinition($entity_type_id);
    }
    else {
      return FALSE;
    }
  }

}
