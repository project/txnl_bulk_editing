<?php

namespace Drupal\txnl_bulk_editing;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for plugin managers that provide allowed field values handlers.
 *
 * @see plugin_api
 */
interface AllowedFieldValuesPluginManagerInterface extends PluginManagerInterface {

  /**
   * Gets the handler to use for the specified field type.
   *
   * @param string $entity_type_id
   *   The entity type containing the target field type.
   * @param string $bundle
   *   The machine name of the bundle containing the field.
   * @param string $field_name
   *   The machine name of the target field.
   *
   * @return \Drupal\txnl_bulk_editing\AllowedFieldValuesPluginInterface|null
   *   If a handler is registered for the field type of the target field, it is
   *   configured and returned; otherwise, null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the handler plugin cannot be instantiated.
   */
  public function getHandlerFor(
      string $entity_type_id,
      string $bundle,
      string $field_name): ?AllowedFieldValuesPluginInterface;

}
