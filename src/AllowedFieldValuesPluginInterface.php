<?php

namespace Drupal\txnl_bulk_editing;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Interface for allowed field values plugins.
 */
interface AllowedFieldValuesPluginInterface extends QueryInterface {

  /**
   * Gets the identifier for this plug-in (as set by its annotation).
   *
   * @return string
   *   The machine name/ID of this plug-in.
   */
  public function id(): string;

  /**
   * Gets whether the provided or logged-in user can edit instances of a field.
   *
   * The target field is the field for which this plug-in has been instantiated
   * to obtain allowed values.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The account for which access is to be checked. If not provided, defaults
   *   to the logged-in user.
   *
   * @return bool
   *   TRUE if the logged-in user has edit access to the target field; or FALSE
   *   if they do not.
   */
  public function doesUserHaveFieldEditAccess(
    AccountInterface $account = NULL): bool;

}
