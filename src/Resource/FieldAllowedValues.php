<?php

namespace Drupal\txnl_bulk_editing\Resource;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Http\Exception\CacheableAccessDeniedHttpException;
use Drupal\Core\Http\Exception\CacheableBadRequestHttpException;
use Drupal\jsonapi\JsonApiResource\ResourceObjectData;
use Drupal\jsonapi\ResourceResponse;
use Drupal\jsonapi\ResourceType\ResourceType;
use Drupal\jsonapi\ResourceType\ResourceTypeAttribute;
use Drupal\jsonapi_resources\Resource\EntityQueryResourceBase;
use Drupal\txnl_bulk_editing\AllowedFieldValuesPluginInterface;
use Drupal\txnl_bulk_editing\AllowedFieldValuesPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Route;

/**
 * Processes a request for the allowed values of an entity field.
 *
 * Currently, only entity reference fields and list (string) fields are
 * supported.
 */
class FieldAllowedValues extends EntityQueryResourceBase implements ContainerInjectionInterface {

  /**
   * The manager of allowed field value handler plug-ins.
   *
   * @var \Drupal\txnl_bulk_editing\AllowedFieldValuesPluginManager
   */
  protected AllowedFieldValuesPluginManager $allowedFieldValuesPluginManager;

  /**
   * Constructs a new FieldAllowedValues object.
   *
   * @param \Drupal\txnl_bulk_editing\AllowedFieldValuesPluginManager $allowed_field_values_plugin_manager
   *   The manager of allowed field value handler plug-ins.
   */
  public function __construct(
      AllowedFieldValuesPluginManager $allowed_field_values_plugin_manager) {
    $this->allowedFieldValuesPluginManager =
      $allowed_field_values_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container): FieldAllowedValues {
    return new static(
      $container->get('plugin.manager.txnl_bulk_editing.allowed_field_values')
    );
  }

  /**
   * Process the resource request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\jsonapi\ResourceType\ResourceType[] $resource_types
   *   The route resource types.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle for the entity.
   * @param string $field_name
   *   The name of the field.
   *
   * @return \Drupal\jsonapi\ResourceResponse
   *   The response.
   */
  public function process(Request $request,
                          array $resource_types,
                          EntityTypeInterface $entity_type,
                          string $bundle,
                          string $field_name): ResourceResponse {
    $cacheability =
      (new CacheableMetadata())->addCacheContexts([
        'url.path',
        'url.query_args:page',
      ]);

    try {
      $query_plugin =
        $this->loadPlugin(
          $entity_type->id(),
          $bundle,
          $field_name,
        );
    }
    catch (\InvalidArgumentException $ex) {
      throw new CacheableBadRequestHttpException(
        $cacheability,
        $ex->getMessage(),
        $ex
      );
    }

    if ($query_plugin === NULL) {
      throw new CacheableBadRequestHttpException(
        $cacheability,
        sprintf(
          'The field "%s" does not support allowed values.',
          $field_name
        )
      );
    }

    // The logged-in user may affect what options are accessible/allowed.
    $cacheability->addCacheContexts(['user']);

    if (!$query_plugin->doesUserHaveFieldEditAccess()) {
      throw new CacheableAccessDeniedHttpException(
        $cacheability,
        sprintf(
          'The logged-in user does not have edit access to the field "%s".',
          $field_name
        )
      );
    }

    $paginator = $this->getPaginatorForRequest($request);
    $paginator->applyToQuery($query_plugin, $cacheability);

    $query_plugin_id = $query_plugin->id();
    $allowed_values  = $query_plugin->execute();

    $resource_type = $resource_types[$query_plugin_id];

    $resources =
      $this->convertValuesToResources(
        $allowed_values,
        $resource_type,
        $cacheability
      );

    $top_level_data = new ResourceObjectData($resources);

    $pagination_links =
      $paginator->getPaginationLinks($query_plugin, $cacheability);

    try {
      return $this->createJsonapiResponse(
        $top_level_data,
        $request,
        200,
        [],
        $pagination_links
      );
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $ex) {
      throw new HttpException(
        Response::HTTP_INTERNAL_SERVER_ERROR,
        'Failed to generate JSON:API response: ' . $ex->getMessage(),
        $ex
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteResourceTypes(Route $route,
                                        string $route_name): array {
    $resource_fields = [
      'value' => new ResourceTypeAttribute('value'),
      'label' => new ResourceTypeAttribute('label'),
    ];

    $plugin_manager = $this->getAllowedFieldValuesPluginManager();
    $plugins        = $plugin_manager->getDefinitions();

    // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $resource_types =
      array_map(
        function (array $plugin_definition) use ($resource_fields) {
          $plugin_id = $plugin_definition['id'];
          assert(!empty($plugin_id));

          $resource_type =
            new ResourceType(
              'field_allowed_value',
              $plugin_id,
              NULL,
              FALSE,
              TRUE,
              TRUE,
              FALSE,
              $resource_fields
            );

          // @todo (ARKNG-993): In the future, try to relate the allowed values
          //   as a relationship in the response.
          $resource_type->setRelatableResourceTypes([]);

          return $resource_type;
        },
        $plugins
      );

    return $resource_types;
  }

  /**
   * Gets the plug-in manager for allowed field values handlers.
   *
   * @return \Drupal\txnl_bulk_editing\AllowedFieldValuesPluginManager
   *   The plug-in manager.
   */
  protected function getAllowedFieldValuesPluginManager(): AllowedFieldValuesPluginManager {
    return $this->allowedFieldValuesPluginManager;
  }

  /**
   * Loads the allowed values plug-in appropriate for the target field.
   *
   * @param string $entity_type_id
   *   The machine name of the target entity type.
   * @param string $bundle
   *   The machine name of the bundle in the entity type.
   * @param string $field_name
   *   The machine name of the target field type.
   *
   * @return \Drupal\txnl_bulk_editing\AllowedFieldValuesPluginInterface|null
   *   The plug-in to use to request allowed values.
   */
  protected function loadPlugin(
      string $entity_type_id,
      string $bundle,
      string $field_name): ?AllowedFieldValuesPluginInterface {
    try {
      $handler =
        $this->getAllowedFieldValuesPluginManager()->getHandlerFor(
          $entity_type_id,
          $bundle,
          $field_name
        );
    }
    catch (PluginException $ex) {
      throw new \RuntimeException(
        'Failed to load allowed field values plug-in: ' . $ex->getMessage(),
        0,
        $ex
      );
    }

    return $handler;
  }

  /**
   * Converts information about allowed values into JSON:API resource objects.
   *
   * @param \Drupal\txnl_bulk_editing\AllowedValue[] $allowed_values
   *   The allowed values to convert.
   * @param \Drupal\jsonapi\ResourceType\ResourceType $resource_type
   *   The JSON:API resource type.
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability for all resource objects.
   *
   * @return \Drupal\jsonapi\JsonApiResource\ResourceObject[]
   *   The resources generated from the allowed values.
   */
  protected function convertValuesToResources(
      array $allowed_values,
      ResourceType $resource_type,
      CacheableDependencyInterface $cacheability): array {
    return array_map(
      function ($allowed_value) use ($resource_type, $cacheability) {
        return $allowed_value->toJsonApiResource($resource_type, $cacheability);
      },
      $allowed_values
    );
  }

}
