<?php

namespace Drupal\txnl_bulk_editing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\txnl_bulk_editing\TransactionInterface;
use Psr\Container\ContainerInterface;

/**
 * Defines a controller to show the Bulk Editor interface.
 */
class BulkEditController extends ControllerBase {

  /**
   * The Drupal route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container): BulkEditController {
    return new static(
      $container->get('router.route_provider'),
    );
  }

  /**
   * Constructor for BulkEditController.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider, which is used for looking up information about what
   *   URL to use when reloading the form.
   */
  public function __construct(RouteProviderInterface $route_provider) {
    $this->routeProvider = $route_provider;
  }

  /**
   * Displays an empty page that React will render the transaction form within.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function bulkEditTransactionForm(
      ?TransactionInterface $transaction): array {
    $rawPath = $this
      ->routeProvider
      ->getRouteByName('txnl_bulk_editing.bulk_edit_transaction.edit')
      ->getPath();
    $edit_page_path = str_replace('{transaction}', '', $rawPath);
    $transaction_id = $transaction === NULL ? NULL : $transaction->uuid();
    return [
      '#markup' => '<div id="root"></div>',
      '#attached' => [
        'library' => [
          'txnl_bulk_editing/bulk_edit_transaction_form',
        ],
        'drupalSettings' => [
          'txnl_bulk_editing' => [
            'editPagePath' => $edit_page_path,
            'transactionId' => $transaction_id,
          ],
        ],
      ],
    ];
  }

}
