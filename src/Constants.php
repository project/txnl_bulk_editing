<?php

namespace Drupal\txnl_bulk_editing;

/**
 * A class containing all constants used in multiple places of this module.
 *
 * Where possible, constants specific to an interface or specific classes should
 * live in those interfaces and classes instead of this class.
 */
abstract class Constants {

  /**
   * Machine name of this module.
   */
  const MODULE_NAME = 'txnl_bulk_editing';

}
