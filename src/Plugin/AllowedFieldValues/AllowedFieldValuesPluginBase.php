<?php

namespace Drupal\txnl_bulk_editing\Plugin\AllowedFieldValues;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\jsonapi_resources\Entity\Query\NonEntityQueryBase;
use Drupal\jsonapi_resources\Entity\Query\PaginatorMetadata;
use Drupal\txnl_bulk_editing\AllowedFieldValuesPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default base class for allowed values handlers that spoof entity queries.
 */
abstract class AllowedFieldValuesPluginBase extends NonEntityQueryBase implements AllowedFieldValuesPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The field definition for which values are being returned.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected FieldDefinitionInterface $fieldDefinition;

  /**
   * The logged-in user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The machine name/ID of this plug-in (set by annotation).
   *
   * @var string
   */
  protected string $id;

  /**
   * Creates an instance of this plug-in.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container, from which services will be obtained.
   * @param array $configuration
   *   The plug-in configuration. This must contain an 'entity_type' value that
   *   specifies the machine name of the entity type that this programmatic
   *   query targets.
   * @param string $plugin_id
   *   The machine name/ID of the plug-in.
   * @param mixed $plugin_definition
   *   The static settings/definition of the plug-in.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   If the plug-in for the target entity type cannot be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the plug-in for the target entity type cannot be found.
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition): AllowedFieldValuesPluginBase {
    return new static(
      $configuration,
      $plugin_id,
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Constructs a new instance.
   *
   * @param array $configuration
   *   The plug-in configuration. This must contain an 'entity_type' value that
   *   specifies the machine name of the entity type that this programmatic
   *   query targets.
   * @param string $plugin_id
   *   The machine name/ID of the plug-in.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The logged-in user account.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   If the plug-in for the target entity type cannot be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the plug-in for the target entity type cannot be found.
   */
  public function __construct(array $configuration,
                              string $plugin_id,
                              EntityTypeManagerInterface $entity_type_manager,
                              AccountInterface $current_user) {
    $entity_type_id   = $configuration['entity_type_id'] ?? NULL;
    $field_definition = $configuration['field_definition'] ?? NULL;

    $this->id = $plugin_id;

    if ($entity_type_id === NULL) {
      throw new \InvalidArgumentException(
        'An "entity_type_id" key must be supplied in plugin configuration.'
      );
    }

    if ($field_definition === NULL) {
      throw new \InvalidArgumentException(
        'An "field_definition" key must be supplied in plugin configuration.'
      );
    }

    $this->fieldDefinition = $field_definition;
    $this->currentUser     = $current_user;

    parent::__construct($entity_type_manager, $entity_type_id);
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function doesUserHaveFieldEditAccess(
      AccountInterface $account = NULL): bool {
    if ($account === NULL) {
      $account = $this->currentUser;
    }

    $field_definition = $this->getFieldDefinition();

    if ($field_definition->getTargetEntityTypeId() !== 'node') {
      // For now, this module can only make bulk updates to nodes.
      $can_access_field = FALSE;
    }
    else {
      $access_control_handler =
        $this->getEntityTypeManager()->getAccessControlHandler(
          $this->getEntityTypeId()
        );

      $access_result =
        $access_control_handler->fieldAccess(
          'edit',
          $field_definition,
          $account,
          NULL,
          TRUE
        );

      // Base fields exist in all entities of the same type; they are not
      // restricted by bundle. So, if we're checking access on a base field of
      // nodes don't need to take the user's per-content-type access
      // restrictions into consideration.
      if (!$field_definition->getFieldStorageDefinition()->isBaseField()) {
        $bundle = $field_definition->getTargetBundle();

        // The user can access this field if they can edit any node of a certain
        // type or can edit nodes they've authored of that type.
        $access_result =
          $access_result->andIf(
            AccessResult::allowedIfHasPermission(
              $account,
              sprintf('edit any %s content', $bundle)
            )->orIf(
              AccessResult::allowedIfHasPermission(
                $account,
                sprintf('edit own %s content', $bundle)
              )
            )
          );
      }

      $can_access_field = $access_result->isAllowed();
    }

    return $can_access_field;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchCountOfResults(): int {
    return count($this->getAllowedValues(0, PHP_INT_MAX));
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\txnl_bulk_editing\AllowedValue[]
   *   Information about each allowed value the logged-on user has access to.
   */
  protected function fetchResults(
      ?PaginatorMetadata $paginator_metadata): array {
    $range = $this->getRange();

    $start_offset = $range['start'] ?? 0;
    $limit        = $range['length'] ?? 50;

    return $this->getAllowedValues($start_offset, $limit);
  }

  /**
   * Queries for the allowed field values.
   *
   * The result must be an array of AllowedFieldValues, containing information
   * about each allowed value the logged-in user has access to. The conditions
   * set on this query and the supplied starting offset and page size limit
   * should be taken into account and applied when generating the results that
   * are returned.
   *
   * @param int $start_offset
   *   The starting offset (starting at zero and relative to the full,
   *   un-paginated result set) for returned values.
   * @param int $page_size
   *   The maximum number of values to return per page of results.
   *
   * @return \Drupal\txnl_bulk_editing\AllowedValue[]
   *   The selection of values allowed by the field that this plug-in handles,
   *   based on the ranges and conditions set in the query.
   */
  abstract protected function getAllowedValues(int $start_offset = 0,
                                               int $page_size = 50): array;

  /**
   * Gets the field definition for which values are being returned.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   The configured field definition.
   */
  protected function getFieldDefinition(): FieldDefinitionInterface {
    return $this->fieldDefinition;
  }

  /**
   * Gets a setting for the given field, while checking the value is present.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition for the field of interest.
   * @param string $setting_name
   *   The machine name for the setting.
   *
   * @return mixed
   *   The value of the setting.
   */
  protected function getRequiredFieldSetting(
      FieldDefinitionInterface $field_definition,
      string $setting_name) {
    $value = $field_definition->getSetting($setting_name);

    if ($value === NULL) {
      throw new \RuntimeException(
        sprintf(
          'The field "%s" is missing required setting "%s".',
          $field_definition->getName(),
          $setting_name
        )
      );
    }

    return $value;
  }

}
