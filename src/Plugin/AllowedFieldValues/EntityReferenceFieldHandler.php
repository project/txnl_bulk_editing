<?php

namespace Drupal\txnl_bulk_editing\Plugin\AllowedFieldValues;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\txnl_bulk_editing\AllowedValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default allowed values handler for entity reference fields.
 *
 * @AllowedFieldValuesHandler(
 *   id="entity_reference",
 *   label="Entity References",
 *   description="Handles node, term, and other entity reference field types.",
 *   field_types={
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceFieldHandler extends AllowedFieldValuesPluginBase {

  /**
   * The manager of entity reference selection plug-ins.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionPluginManager;

  /**
   * Creates an instance of this plug-in.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container, from which services will be obtained.
   * @param array $configuration
   *   The plug-in configuration. This must contain an 'entity_type' value that
   *   specifies the machine name of the entity type that this programmatic
   *   query targets.
   * @param string $plugin_id
   *   The machine name/ID of the plug-in.
   * @param mixed $plugin_definition
   *   The static settings/definition of the plug-in.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   If the plug-in for the target entity type cannot be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the plug-in for the target entity type cannot be found.
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition): EntityReferenceFieldHandler {
    return new static(
      $configuration,
      $plugin_id,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('plugin.manager.entity_reference_selection')
    );
  }

  /**
   * Constructor for EntityReferenceFieldHandler.
   *
   * @param array $configuration
   *   The plug-in configuration. This must contain an 'entity_type' value that
   *   specifies the machine name of the entity type that this programmatic
   *   query targets.
   * @param string $plugin_id
   *   The machine name/ID of the plug-in.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The logged-in user account.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_plugin_manager
   *   The manager of entity reference selection plug-ins.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   If the plug-in for the target entity type cannot be loaded.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the plug-in for the target entity type cannot be found.
   */
  public function __construct(
      array $configuration,
      string $plugin_id,
      EntityTypeManagerInterface $entity_type_manager,
      AccountInterface $current_user,
      SelectionPluginManagerInterface $selection_plugin_manager) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $entity_type_manager,
      $current_user
    );

    $this->selectionPluginManager = $selection_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedValues(int $start_offset = 0,
                                   int $page_size = 50): array {
    $field_allowed_values = [];

    $field_definition = $this->getFieldDefinition();

    $target_entity_type =
      $this->getRequiredFieldSetting($field_definition, 'target_type');

    $selection_handler =
      $this->selectionPluginManager->getSelectionHandler($field_definition);

    try {
      $target_entity_storage =
        $this->getEntityTypeManager()->getStorage($target_entity_type);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $ex) {
      throw new \RuntimeException(
        sprintf(
          'Failed to load storage for target entity type ("%s"): %s',
          $target_entity_type,
          $ex->getMessage()
        ),
        0,
        $ex
      );
    }

    $referenceable_entities = $selection_handler->getReferenceableEntities();

    $entity_ids_to_load =
      array_reduce(
        $referenceable_entities,
        function ($current_entity_ids, $bundle_entities) {
          return array_merge($current_entity_ids, array_keys($bundle_entities));
        },
        []
      );

    $id_field = $target_entity_storage->getEntityType()->getKey('id');

    // @todo (ARKNG-992): Eliminate this and use paging offset once DDO-3001122
    //   is available.
    $entity_ids_to_load =
      $target_entity_storage
        ->getQuery()
        ->condition($id_field, $entity_ids_to_load, 'IN')
        ->range($start_offset, $page_size)
        ->execute();

    $loaded_entities =
      $target_entity_storage->loadMultiple(array_values($entity_ids_to_load));

    foreach ($loaded_entities as $entity) {
      $uuid = $entity->uuid();

      $field_allowed_values[] =
        new AllowedValue($uuid, $uuid, $entity->label(), $entity);
    }

    return $field_allowed_values;
  }

  /**
   * Gets the entity reference selection plug-in manager.
   *
   * @return \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   *   The selection plug-in manager.
   */
  protected function getSelectionPluginManager(): SelectionPluginManagerInterface {
    return $this->selectionPluginManager;
  }

}
