<?php

namespace Drupal\txnl_bulk_editing;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\jsonapi\JsonApiResource\LinkCollection;
use Drupal\jsonapi\JsonApiResource\ResourceObject;
use Drupal\jsonapi\ResourceType\ResourceType;

/**
 * A single allowed value for a field.
 */
class AllowedValue {

  /**
   * The Universally Unique Identifier (UUID) for this allowed field value.
   *
   * @var string
   */
  protected string $uuid;

  /**
   * The machine name/stored version of this value, as appears in the database.
   *
   * @var string
   */
  protected string $value;

  /**
   * The human-friendly name for this value.
   *
   * @var string
   */
  protected string $label;

  /**
   * The entity that is the authority on the value.
   *
   * Caching of the value will depend on this entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $relatedEntity;

  /**
   * Constructs a new instance.
   *
   * @param string $uuid
   *   The Universally Unique Identifier (UUID) for this allowed field value.
   * @param string $value
   *   The machine name/stored version of this value, as appears in the
   *   database.
   * @param string $label
   *   The human-friendly name for this value.
   * @param \Drupal\Core\Entity\EntityInterface $related_entity
   *   The entity that is the authority on the value.
   */
  public function __construct(string $uuid,
                              string $value,
                              string $label,
                              EntityInterface $related_entity) {
    $this->uuid          = $uuid;
    $this->value         = $value;
    $this->label         = $label;
    $this->relatedEntity = $related_entity;
  }

  /**
   * Gets the Universally Unique Identifier (UUID) for this allowed field value.
   *
   * @return string
   *   The UUID.
   */
  public function getUuid(): string {
    return $this->uuid;
  }

  /**
   * Gets the machine name/stored version of this value.
   *
   * @return string
   *   This value, as it appears in the database.
   */
  public function getValue(): string {
    return $this->value;
  }

  /**
   * Gets the human-friendly name for this value.
   *
   * @return string
   *   The human-friendly value label.
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * Gets the entity that is the authority on the value.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The related entity.
   */
  public function getRelatedEntity(): EntityInterface {
    return $this->relatedEntity;
  }

  /**
   * Converts this allowed value into a JSON:API resource object.
   *
   * @param \Drupal\jsonapi\ResourceType\ResourceType $resource_type
   *   The resource type definition that defines what type of JSON:API entity
   *   the resource represents.
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability for the resource object.
   *
   * @return \Drupal\jsonapi\JsonApiResource\ResourceObject
   *   The allowed value, as a resource object.
   */
  public function toJsonApiResource(
      ResourceType $resource_type,
      CacheableDependencyInterface $cacheability): ResourceObject {
    $links = new LinkCollection([]);

    $cacheability =
      CacheableMetadata::createFromObject($cacheability)
        ->addCacheableDependency($this->getRelatedEntity());

    return new ResourceObject(
      $cacheability,
      $resource_type,
      $this->getUuid(),
      NULL,
      [
        'value' => $this->getValue(),
        'label' => $this->getLabel(),
      ],
      $links
    );
  }

}
