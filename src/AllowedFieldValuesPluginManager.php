<?php

namespace Drupal\txnl_bulk_editing;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a plugin manager for allowed field values handlers.
 *
 * @see plugin_api
 */
class AllowedFieldValuesPluginManager extends DefaultPluginManager implements AllowedFieldValuesPluginManagerInterface {

  /**
   * The Drupal entity type manager service, for loading entity type info.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Drupal entity type manager service, for loading field info.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new AllowedValuesPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager service, for loading entity type info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The Drupal entity field manager service, for loading field info.
   */
  public function __construct(
      \Traversable $namespaces,
      CacheBackendInterface $cache_backend,
      ModuleHandlerInterface $module_handler,
      EntityTypeManagerInterface $entity_type_manager,
      EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct(
      'Plugin/AllowedFieldValues',
      $namespaces,
      $module_handler,
      'Drupal\txnl_bulk_editing\AllowedFieldValuesPluginInterface',
      'Drupal\txnl_bulk_editing\Annotation\AllowedFieldValuesHandler'
    );

    $this->alterInfo('txnl_bulk_editing_allowed_values_handler_info');

    $this->setCacheBackend(
      $cache_backend,
      'txnl_bulk_editing_plugins_allowed_values'
    );

    $this->entityTypeManager  = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function getHandlerFor(
      string $entity_type_id,
      string $bundle,
      string $field_name): ?AllowedFieldValuesPluginInterface {
    $instance = NULL;

    $definitions = $this->getDefinitions();

    $field_definition =
      $this->getFieldDefinition($entity_type_id, $bundle, $field_name);

    $field_type_id = $field_definition->getType();

    foreach ($definitions as $plugin_id => $definition) {
      $supported_field_types = $definition['field_types'] ?? [];

      if (in_array($field_type_id, $supported_field_types)) {
        $plugin_configuration = [
          'entity_type_id'   => $entity_type_id,
          'bundle'           => $bundle,
          'field_name'       => $field_name,
          'field_definition' => $field_definition,
        ];

        $instance = $this->createInstance($plugin_id, $plugin_configuration);
        assert($instance instanceof AllowedFieldValuesPluginInterface);

        break;
      }
    }

    return $instance;
  }

  /**
   * Gets the definition for the specified field.
   *
   * @param string $entity_type_id
   *   Machine name of the entity type.
   * @param string $bundle
   *   Machine name of the bundle.
   * @param string $field_name
   *   Machine name of the field.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   The definition for the specified field.
   *
   * @throws \InvalidArgumentException
   *   If the specified field cannot be found.
   */
  protected function getFieldDefinition(
      string $entity_type_id,
      string $bundle,
      string $field_name): FieldDefinitionInterface {
    $field_definitions =
      $this->entityFieldManager->getFieldDefinitions(
        $entity_type_id,
        $bundle
      );

    $field_definition = $field_definitions[$field_name] ?? NULL;

    if ($field_definition === NULL) {
      throw new \InvalidArgumentException(
        sprintf(
          'The field "%s" does not exist.',
          implode('.', [$entity_type_id, $bundle, $field_name])
        )
      );
    }

    return $field_definition;
  }

}
