<?php

namespace Drupal\txnl_bulk_editing\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines the annotation for allowed field values handlers.
 *
 * @Annotation
 */
class AllowedFieldValuesHandler extends Plugin {

  /**
   * The plugin ID for this handler.
   *
   * @var string
   */
  public string $id;

  /**
   * An array of field types the handler supports.
   *
   * @var array
   */
  public array $field_types = [];

  /**
   * The human-readable name of this handler.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * A short human readable description for this handler.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

}
