<?php

namespace Drupal\txnl_bulk_editing;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for "Bulk Edit Transaction" entities.
 *
 * Bulk edit transactions are used to make wide-sweeping, surgically-precise
 * edits to multiple nodes in bulk, while providing a way to track and
 * optionally roll back those edits at a later time.
 */
interface TransactionInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the transaction label.
   *
   * @return string
   *   Label of the transaction.
   */
  public function getLabel(): string;

  /**
   * Machine name of the status for transaction entities when pending.
   *
   * A pending bulk edit transaction is one that can still be edited by a user.
   */
  const TRANSACTION_STATUS_PENDING = 'pending';

  /**
   * Machine name of the status for transaction entities when running.
   *
   * A running bulk edit transaction can no longer be edited and is in the
   * process of being applied to target nodes. It cannot yet be rolled back.
   */
  const TRANSACTION_STATUS_RUNNING = 'running';

  /**
   * Machine name of the status for transaction entities when completed.
   *
   * A completed bulk edit transaction can no longer be edited. It has already
   * been applied to target nodes and can be rolled back.
   */
  const TRANSACTION_STATUS_COMPLETED = 'completed';

  /**
   * Machine name of the status for transaction entities when rolled back.
   *
   * A rolled-back bulk edit transaction can no longer be edited or rolled back
   * a second time. It has already been applied to target nodes and then rolled
   * back.
   */
  const TRANSACTION_STATUS_ROLLED_BACK = 'rolled_back';

}
