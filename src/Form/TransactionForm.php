<?php

namespace Drupal\txnl_bulk_editing\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inveniem_utilities\Utility\ErrorHandler;

/**
 * Form controller for the transaction entity edit forms.
 */
class TransactionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();

    $messenger = $this->messenger();
    $logger    = $this->logger('txnl_bulk_editing');

    try {
      $result = $entity->save();
      $link = $entity->toLink($this->t('View'))->toRenderable();

      $message_arguments = ['%label' => $this->entity->label()];
      $logger_arguments = $message_arguments + ['link' => render($link)];

      if ($result == SAVED_NEW) {
        $messenger->addStatus(
          $this->t(
            'New transaction %label has been created.',
            $message_arguments
          )
        );

        $logger->notice('Created new transaction %label', $logger_arguments);
      }
      else {
        $messenger->addStatus(
          $this->t(
            'The transaction %label has been updated.',
            $message_arguments
          )
        );

        $logger->notice('Updated new transaction %label.', $logger_arguments);
      }

      $form_state->setRedirect(
        'entity.txnl_bulk_editing.canonical',
        ['transaction' => $entity->id()]
      );
    }
    catch (EntityStorageException | EntityMalformedException $e) {
      $messenger->addError(
        $this->t(
          'Something went wrong saving this transaction. Please try again.'
        )
      );

      // In the unlikely case something went wrong on save, we need to keep the
      // user on the form.
      $form_state->setRebuild();

      ErrorHandler::logException($logger, $e, 'Failed to save transaction.');
    }
  }

}
