<?php

namespace Drupal\txnl_bulk_editing\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\txnl_bulk_editing\TransactionInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the transaction entity class.
 *
 * @ContentEntityType(
 *   id = "bulk_edit_transaction",
 *   label = @Translation("Bulk edit transaction"),
 *   label_collection = @Translation("Bulk edit transactions"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\txnl_bulk_editing\TransactionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\txnl_bulk_editing\Form\TransactionForm",
 *       "edit" = "Drupal\txnl_bulk_editing\Form\TransactionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "bulk_edit_transaction",
 *   data_table = "bulk_edit_transaction_field_data",
 *   translatable = TRUE,
 *   admin_permission = "create bulk edit transactions",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/bulk-edit/transactions/add",
 *     "canonical" = "/admin/content/bulk-edit/transactions/{bulk_edit_transaction}",
 *     "edit-form" = "/admin/content/bulk-edit/transactions/{bulk_edit_transaction}/edit",
 *     "delete-form" = "/admin/content/bulk-edit/transactions/{bulk_edit_transaction}/delete",
 *     "collection" = "/admin/content/bulk-edit/transactions"
 *   },
 *   field_ui_base_route = "entity.txnl_bulk_editing.settings"
 * )
 */
class Transaction extends ContentEntityBase implements TransactionInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    $author = $this->getOwner()->getDisplayName();
    $created = $this->get('created')->value;
    $format = '%s - %s';
    return sprintf($format, $author, $created);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setDescription(t('The Universally Unique Identifer (UUID) for the transaction.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the transaction was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the transaction was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setTranslatable(TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of this transaction.'))
      ->setDefaultValue(TransactionInterface::TRANSACTION_STATUS_PENDING)
      ->setSettings([
        'allowed_values' => [
          TransactionInterface::TRANSACTION_STATUS_PENDING => 'Pending',
          TransactionInterface::TRANSACTION_STATUS_RUNNING => 'Running',
          TransactionInterface::TRANSACTION_STATUS_COMPLETED => 'Completed',
          TransactionInterface::TRANSACTION_STATUS_ROLLED_BACK => 'Rolled back',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['query'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Query'))
      ->setDescription(t('The SQ expression for locating nodes that the transaction should affect.'))
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'text_default',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
