import { defineConfig, ConfigEnv, UserConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";

export default defineConfig(
  ({ mode }: ConfigEnv): UserConfig => ({
    build: {
      // The Drupal documentation recommends that we let Drupal take care of
      // minification.
      //
      // https://www.drupal.org/docs/8/api/translation-api/overview#s-translation-in-javascript-files
      minify: false,

      // Although assets are not minified, they are bundled, so sourcemaps are
      // still useful.
      sourcemap: mode !== "production",

      rollupOptions: {
        input: ["react/main.tsx"],

        // By default, Vite names bundles according to this pattern:
        //
        //   assets/[name].[hash].[ext]
        //
        // However, we need the names to be unchanging so that we can refer to
        // them in the .libraries.yml file. Drupal will add cache-busting
        // characters to the bundles it creates for production.
        output: {
          entryFileNames: "assets/[name].js",
          chunkFileNames: "assets/[name].js",
          assetFileNames: "assets/[name].[ext]",
        },
      },
    },
    plugins: [tsconfigPaths()],
  })
);
