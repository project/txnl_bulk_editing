# Transactional Bulk Editing

Transactional Bulk Editing is a Drupal module which provides a user interface
for making wide-sweeping, surgically-precise edits to multiple nodes in bulk,
while providing a way to track and optionally roll back those edits at a later
time.

It is recommended that users who are granted the "create bulk edit transactions"
permission are also granted the "view the administration theme" permission. The
Transactional Bulk Editing form is styled to look best within Drupal's Seven
theme.
