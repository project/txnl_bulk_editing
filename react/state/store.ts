import { configureStore } from "@reduxjs/toolkit";
import {
  reducer as beesReducer,
  middleware as beesMiddleware,
  // @ts-expect-error: No types are published for redux-bees.
  // https://github.com/Inveniem/redux-bees/issues/3
} from "@inveniem/redux-bees";

import DrupalAPI from "../api";

import allowedValuesByFieldNameReducer from "./slices/allowedValuesByFieldName";
import fieldsReducer from "./slices/fields";
import fieldGroupsReducer from "./slices/fieldGroups";
import fieldStorageConfigReducer from "./slices/fieldStorageConfig";
import formStatusReducer from "./slices/formStatus";
import persistenceReducer from "./slices/persistence";
import previouslySavedTransactionReducer from "./slices/previouslySavedTransaction";
import resultsReducer from "./slices/results";

let drupalAPI: DrupalAPI;

/**
 * Initialize the DrupalAPI with the provided base URL.
 *
 * @param {string} baseURL
 *   The base URL for the Drupal deployment.
 */
export function initializeAPI(baseURL: string): void {
  drupalAPI = new DrupalAPI(baseURL);
}

/**
 * The single, global Redux store, which combines the reducers provided by
 * individual slices.
 */
const store = configureStore({
  reducer: {
    allowedValuesByFieldName: allowedValuesByFieldNameReducer,
    formStatus: formStatusReducer,
    fields: fieldsReducer,
    fieldGroups: fieldGroupsReducer,
    fieldStorageConfig: fieldStorageConfigReducer,
    persistence: persistenceReducer,
    previouslySavedTransaction: previouslySavedTransactionReducer,
    results: resultsReducer,
    bees: beesReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: {
          // We need to provide a function for *retrieving* `drupalAPI`, rather
          // than providing `drupalAPI` itself, because the store is created
          // before initializeAPI is called, while drupalAPI is still undefined.
          //
          // Notice, in main.jsx, that initializeAPI can only be called when the
          // Drupal behavior runs, and the Drupal behavior runs asynchronously.
          getDrupalAPI: () => drupalAPI,
        },
      },

      // beesReducer is not serializable.
      // https://github.com/Inveniem/redux-bees/issues/4
      serializableCheck: false,
    }).concat(beesMiddleware()),
});

/**
 * Types which can be used in components. See the {@link
 * https://redux-toolkit.js.org/usage/usage-with-typescript#configurestore Redux
 * Toolkit documentation about using createStore in TypeScript} for more
 * information.
 */
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
