import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { FieldType } from "@custom-types/fields";
import { Fetchable, FetchStatus } from "@custom-types/state";
import { fetchPaginatedAttributes } from "@utils/json-api";

import { fetchFieldStorageConfig } from "./fieldStorageConfig";

import type DrupalAPI from "../../api";
import type { FieldStorageConfig } from "./fieldStorageConfig";
import type { AppDispatch, RootState } from "../store";

// Field data as returned by the Drupal API.
export type RawField = {
  description?: string;
  field_name: string;
  field_type: string;
  label: string;
  settings?: {
    handler?: string;
    handler_settings?: {
      target_bundles?: {
        [key: string]: string;
      };
    };
  };
};

// Formatted field data for use in other parts of the application.
export type Field = {
  cardinality: number | null;
  description: string | null;
  label: string;
  name: string;
  resourceType: string;
  type: FieldType;
};

export type Fields = {
  [fieldName: string]: Field;
};

const initialState: Fetchable<Fields> = {
  status: FetchStatus.Idle,
  data: null,
};

/**
 * Return the cardinality of the given field, or null if it has no cardinality.
 *
 * @param {RawField} rawField
 *   Raw fields as returned by the Drupal API.
 * @param {FieldStorageConfig[]} fieldStorageConfig
 *   Field storage config loading status and data.
 *
 * @return {number | null}
 *   The cardinality of the given field, or null if it has no cardinality.
 */
function getFieldCardinality(
  rawField: RawField,
  fieldStorageConfig: FieldStorageConfig[]
): number | null {
  return (
    fieldStorageConfig.find(
      (fsc: FieldStorageConfig) => fsc.field_name === rawField.field_name
    )?.cardinality || null
  );
}

/**
 * Return the resource type of the specified field, where a resource type is the
 * entity type and the bundle separated by two dashes, for example,
 * `taxonomy_term--artifact_subject` or `taxonomy_term--box`. If a resource type
 * cannot be determined, return null.
 *
 * @param {RawField} rawField
 *   The raw field whose resource type should be returned.
 *
 * @return {string}
 *   The resource type of the field with the given name, or the string
 *   "UNKNOWN_RESOURCE_TYPE" if the resource type cannot be determined.
 */
function getFieldResourceType(rawField: RawField): string {
  const fieldSettings = rawField.settings;
  const fallback: string = "UNKNOWN_RESOURCE_TYPE";

  if (
    fieldSettings?.handler === undefined ||
    fieldSettings?.handler_settings?.target_bundles === undefined
  ) {
    return fallback;
  }

  const entityTypeRegex: RegExp = /.*:(.*)/;
  const targetBundlesKeys: string[] = Object.keys(
    fieldSettings.handler_settings.target_bundles
  );

  if (
    !entityTypeRegex.test(fieldSettings.handler) ||
    !targetBundlesKeys.length
  ) {
    return fallback;
  }

  const entityType: string = fieldSettings.handler.replace(
    entityTypeRegex,
    "$1"
  );
  const bundle: string = targetBundlesKeys[0];

  return `${entityType}--${bundle}`;
}

/**
 * Return the field type of the given field.
 *
 * @param {RawField} rawField
 *   Raw configuration for a field as returned by the Drupal API.
 * @param {number | null} cardinality
 *   The cardinality of the given field, or null if it has no cardinality.
 *
 * @return {FieldType}
 *   The type of the given field.
 */
function getFieldType(
  rawField: RawField,
  cardinality: number | null
): FieldType {
  if (rawField.field_type !== "entity_reference" || cardinality === null) {
    return FieldType.Unknown;
  }

  if (cardinality === 1) {
    return FieldType.SingleValueTaxonomy;
  }

  // An `entity_reference` field with a cardinality of -1 supports infinitely
  // many terms.
  if (cardinality > 1 || cardinality === -1) {
    return FieldType.MultipleValueTaxonomy;
  }

  return FieldType.Unknown;
}

/**
 * Unmarshall raw fields, as returned by the Drupal API, to a structure that is
 * easier to work with in other parts of the application.
 *
 * @param {RawField[]} rawFields
 *   Raw fields as returned by the Drupal API.
 * @param {FieldStorageConfig[]} fieldStorageConfig
 *   Field storage config loading status and data.
 *
 * @return {Fields}
 *   Field data in a structure that is easier to work with in other parts of the
 *   application.
 */
function unmarshallRawFields(
  rawFields: RawField[],
  fieldStorageConfig: FieldStorageConfig[]
): Fields {
  return rawFields.reduce((acc: Fields, rawField: RawField) => {
    const cardinality: number | null = getFieldCardinality(
      rawField,
      fieldStorageConfig
    );

    acc[rawField.field_name] = {
      cardinality,
      description: rawField.description || null,
      label: rawField.label,
      name: rawField.field_name,
      resourceType: getFieldResourceType(rawField),
      type: getFieldType(rawField, cardinality),
    };

    return acc;
  }, {});
}

/**
 * Redux thunk which fetches and formats fields.
 */
export const fetchFields = createAsyncThunk<
  Fields,
  undefined,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/fetchFieldsStatus",
  async (args: undefined, thunkAPI): Promise<Fields> => {
    await thunkAPI.dispatch<any>(fetchFieldStorageConfig());

    const drupalAPI: DrupalAPI = thunkAPI.extra.getDrupalAPI();

    // We can coerce the `FieldStorageConfig[]` type because we know it will not
    // be null unless a server error is encountered.
    //
    // TODO (ARKNG-1062): Handle server errors.
    const fieldStorageConfig: FieldStorageConfig[] = thunkAPI.getState()
      .fieldStorageConfig.data as FieldStorageConfig[];

    // TODO (ARKNG-1002): Do not assume filter values.
    //
    // Do not assume a machine name of "artifact" or an entity type of "node",
    // as these cannot be safely assumed on other sites which use this module
    // after it is released to the open-source community.
    const requestArgs = {
      "filter[entity_type]": "node",
      "filter[bundle]": "artifact",
    };

    const rawFields = await fetchPaginatedAttributes<RawField>(
      thunkAPI.dispatch,
      thunkAPI.getState,
      drupalAPI.getFields,
      requestArgs
    );

    // Manually add relevant configuration data about base fields, since the
    // JSON:API endpoint does not include configuration information about
    // them.
    //
    // https://www.drupal.org/docs/drupal-apis/entity-api/defining-and-using-content-entity-field-definitions#s-base-fields
    //
    // TODO (ARKNG-1016): Generate and fetch the title field configuration from
    // the TBE back-end.
    const extraFields = [
      {
        field_name: "title",
        field_type: "string",
        label: "Title",
      },
    ];

    return unmarshallRawFields(
      extraFields.concat(rawFields),
      fieldStorageConfig
    );
  }
);

/**
 * Redux slice which stores field configurations.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
const fieldsSlice = createSlice({
  name: "fields",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchFields.pending, (state) => {
      if (state.status === FetchStatus.Idle) {
        state.status = FetchStatus.Pending;
      }
    });
    builder.addCase(fetchFields.fulfilled, (state, action) => {
      if (state.status === FetchStatus.Pending) {
        state.status = FetchStatus.Fulfilled;
        state.data = action.payload;
      }
    });
    builder.addCase(fetchFields.rejected, (state) => {
      if (state.status === FetchStatus.Pending) {
        state.status = FetchStatus.Rejected;
      }
    });
  },
});

/**
 * Redux selector for retrieving field configuration loading status and
 * formatted data, if any.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {Fetchable<Fields>}
 *   Field loading status and formatted data, if any.
 */
export const selectFields = (state: RootState): Fetchable<Fields> =>
  state.fields;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default fieldsSlice.reducer;
