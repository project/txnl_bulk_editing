import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import type { ResourceObject, ResourceObjects } from "ts-json-api";
import type DrupalAPI from "../../api";
import type { AppDispatch, RootState } from "../store";

export type Value = {
  label: string;
  id: string;
};

export type AllowedValuesByFieldName = {
  [fieldName: string]: Value[];
};

// Don't include loading status for now. `fetchAllowedValues` can be called
// multiple times, so it's not clear what loading status should mean.
//
// In the future, if we need to know when a specific field's allowed values have
// been loaded, we could add support for that.
type State = {
  data: AllowedValuesByFieldName;
};

const initialState: State = {
  data: {},
};

/**
 * Redux thunk which fetches allowed values.
 */
export const fetchAllowedValues = createAsyncThunk<
  AllowedValuesByFieldName,
  string,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/getAllowedValuesStatus",
  async (fieldName: string, thunkAPI): Promise<AllowedValuesByFieldName> => {
    const state = thunkAPI.getState();
    const allowedValues = state.allowedValuesByFieldName.data[fieldName];

    // Don't fetch the allowed values for this field if we already have.
    if (allowedValues !== undefined) {
      return {
        [fieldName]: allowedValues,
      };
    }

    const drupalAPI: DrupalAPI = thunkAPI.extra.getDrupalAPI();

    const response = await drupalAPI.getAllowedValues(
      thunkAPI.dispatch,
      thunkAPI.getState,

      // TODO (ARKNG-1002): Do not assume a machine name of "artifact" or an
      // entity type of "node", as these cannot be safely assumed on other
      // sites which use this module after it is released to the open-source
      // community.
      {
        entityType: "node",
        bundle: "artifact",
        fieldName,
      }
    );

    return {
      [fieldName]: response.body.data.reduce(
        (acc: ResourceObjects, datum: ResourceObject) => {
          const label = datum?.attributes?.label;
          const id = datum?.attributes?.value;

          if (typeof label !== "string" || typeof id !== "string") return acc;

          return [...acc, { label, id }];
        },
        []
      ),
    };
  }
);

/**
 * Redux slice which stores allowed values by field name.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
const allowedValuesByFieldNameSlice = createSlice({
  name: "allowedValuesByFieldName",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAllowedValues.fulfilled, (state, action) => {
      Object.entries(action.payload).forEach(
        ([fieldName, allowedValues]: [string, Value[]]) => {
          state.data[fieldName] = allowedValues;
        }
      );
    });
  },
});

/**
 * Redux selector for retrieving allowed values by field name.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {AllowedValuesByFieldName}
 *   Allowed values by field name.
 */
export const selectAllowedValuesByFieldName = (
  state: RootState
): AllowedValuesByFieldName => state.allowedValuesByFieldName.data;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default allowedValuesByFieldNameSlice.reducer;
