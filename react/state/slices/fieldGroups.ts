import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Fetchable, FetchStatus } from "@custom-types/state";

import mockFieldGroupsConfigurations from "../../../../../../../mock-data/entity_form_display.json";

import type { AppDispatch, RootState } from "../store";

export type FieldGroup = {
  weight: number;
};

export type FieldGroups = {
  [groupName: string]: FieldGroup;
};

const initialState: Fetchable<FieldGroups> = {
  status: FetchStatus.Idle,
  data: null,
};

/**
 * Redux thunk which fetches field group configurations.
 *
 * TODO (ARKNG-878): Re-implement this once the back-end provides real field
 * group data. Some of the code that lived in the EditForm.tsx component in old
 * commits may be helpful.
 */
export const fetchFieldGroups = createAsyncThunk<
  FieldGroups,
  undefined,
  {
    dispatch: AppDispatch;
    state: RootState;
  }
>("fields/fetchFieldGroupsStatus", async (): Promise<FieldGroups> => {
  const response = mockFieldGroupsConfigurations;
  return response.data[0].attributes.third_party_settings.field_group;
});

/**
 * Redux slice which stores field group configurations.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
const fieldGroupsSlice = createSlice({
  name: "fieldGroups",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchFieldGroups.pending, (state) => {
      if (state.status === FetchStatus.Idle) {
        state.status = FetchStatus.Pending;
      }
    });
    builder.addCase(fetchFieldGroups.fulfilled, (state, action) => {
      if (state.status === FetchStatus.Pending) {
        state.status = FetchStatus.Fulfilled;
        state.data = action.payload;
      }
    });
    builder.addCase(fetchFieldGroups.rejected, (state) => {
      state.status = FetchStatus.Rejected;
    });
  },
});

/**
 * Redux selector for retrieving field group configuration loading status and
 * data, if any.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {Fetchable<FieldGroups>}
 *   Field group configuration loading status and data, if any.
 */
export const selectFieldGroups = (state: RootState): Fetchable<FieldGroups> =>
  state.fieldGroups;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default fieldGroupsSlice.reducer;
