import { createSlice } from "@reduxjs/toolkit";

import type { PayloadAction, Draft } from "@reduxjs/toolkit";
import type { RootState } from "../store";

export type EditsState = {
  [editName: number]: {
    restored?: boolean;
    previouslyUsedUUID?: string;
  };
};

type SetEditRestoredAction = {
  editName: number;
  value: boolean;
};

type SetPreviouslyUsedEditUUIDAction = {
  editName: number;
  value: string;
};

type FormStatusState = {
  saveEnabled: boolean;
  edits: EditsState;
};

const initialState: FormStatusState = {
  saveEnabled: false,
  edits: {},
};

/**
 * Redux slice which stores the status of the "Transactional bulk editing" form
 * and which provides methods for updating that status.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 *
 * Note that this slice does not and should not store form values themselves.
 * The Redux documentation {@link
 * https://redux.js.org/style-guide/style-guide#avoid-putting-form-state-in-redux
 * recommends against storing form values in Redux}.
 */
export const formStatusSlice = createSlice({
  name: "formStatus",
  initialState,
  reducers: {
    setSaveEnabled: (
      state: Draft<FormStatusState>,
      action: PayloadAction<boolean>
    ): void => {
      state.saveEnabled = action.payload;
    },
    setEditRestored: (
      state: Draft<FormStatusState>,
      action: PayloadAction<SetEditRestoredAction>
    ): void => {
      const { editName }: { editName: number } = action.payload;
      if (state.edits[editName] === undefined) {
        state.edits[editName] = {};
      }
      state.edits[editName].restored = action.payload.value;
    },
    setPreviouslyUsedEditUUID: (
      state: Draft<FormStatusState>,
      action: PayloadAction<SetPreviouslyUsedEditUUIDAction>
    ): void => {
      const { editName }: { editName: number } = action.payload;
      if (state.edits[editName] === undefined) {
        state.edits[editName] = {};
      }
      state.edits[editName].previouslyUsedUUID = action.payload.value;
    },
  },
});

/**
 * Redux action for updating whether the form should allow saving.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {boolean} action
 *   An action containing a boolean value indicating whether the form should
 *   allow saving.
 * @type {function}
 */
export const { setSaveEnabled } = formStatusSlice.actions;

/**
 * Redux action for indicating whether an edit has been restored.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {SetEditRestoredAction} action
 *   An action containing the name of an edit and a boolean value indicating
 *   whether it has been restored.
 * @type {function}
 */
export const { setEditRestored } = formStatusSlice.actions;

/**
 * Redux action for setting the previously used UUID of an edit.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {SetEditRestoredAction} action
 *   An action containing the previously used UUID for an edit.
 * @type {function}
 */
export const { setPreviouslyUsedEditUUID } = formStatusSlice.actions;

/**
 * Redux selector for retrieving whether the form should allow saving.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {boolean}
 *   `true` if the save button is enabled, otherwise `false`.
 */
export const selectSaveEnabled = (state: RootState): boolean =>
  state.formStatus.saveEnabled;

/**
 * Redux selector for retrieving information about edits, including restoration
 * status and previously used UUID, if any.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {EditsState}
 *   Information about edits, including restoration status and previously used
 *   UUID, if any.
 */
export const selectEditsState = (state: RootState): EditsState =>
  state.formStatus.edits;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default formStatusSlice.reducer;
