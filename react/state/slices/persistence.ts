import {
  createAsyncThunk,
  createSlice,
  Draft,
  PayloadAction,
} from "@reduxjs/toolkit";
import { Fetchable } from "@custom-types/state";
import {
  FormValuesDirector,
  RestoreTransactionFormValuesBuilder,
} from "@utils/builders/form-values";
import {
  RequestDirector,
  SaveNewTransactionRequestBuilder,
  UpdateExistingTransactionRequestBuilder,
} from "@utils/builders/requests";
import { getFetchableWithRetries } from "@utils/loading";

import DrupalAPI, { ENTITY_TYPE_BULK_EDIT_TRANSACTION } from "../../api";

import { setPreviouslyUsedEditUUID, setSaveEnabled } from "./formStatus";

import type { FormInstance } from "antd/lib/form";
import type { Request, Response } from "ts-json-api";
import type {
  BulkEditResourceObject,
  NewBulkEditResourceObject,
} from "@custom-types/api";
import type { FormValues } from "@custom-types/form";
import type { AppDispatch, RootState } from "../store";
import type { Fields } from "./fields";

export type TransactionID = string;
export type TransactionTime = number;

type SetEgocentricTimeLastSavedAction = TransactionTime;
type SetTransactionIDAction = TransactionID;

export type SaveReport = {
  id: TransactionID;
  time: TransactionTime;
};

type State = {
  transactionID: TransactionID | null;
  egocentricTimeLastSaved: TransactionTime | null;
};

let timesGetTransactionCalled = 0;

const initialState: State = {
  transactionID: null,

  // `egocentricTimeLastSaved` is the time that the bulk edit is thought to have
  // most recently been saved, from the point of view of this session. If, when
  // a save of the bulk edit is attempted, the server reports that it was
  // actually most recently saved at a different time, then it must have been
  // saved somewhere else, perhaps in another tab or by another user.
  //
  // The value is initially unknown, but is immediately fetched. It is also
  // updated whenever a save is successful in this session.
  egocentricTimeLastSaved: null,
};

/**
 * Marshall form values to a request that can be passed to the API to save a new
 * transaction.
 *
 * @param {FormValues} formValues
 *   Form values for the new bulk edit transaction that should be saved.
 * @param {Fields} fields
 *   Formatted field configuration data.
 * @return {Request<NewBulkEditResourceObject>}
 *   A marshalled request which can be passed to the API to save a new
 *   transaction.
 */
function marshallSaveNewTransactionRequest(
  formValues: FormValues,
  fields: Fields
): Request<NewBulkEditResourceObject> {
  const requestBuilder = new SaveNewTransactionRequestBuilder(
    ENTITY_TYPE_BULK_EDIT_TRANSACTION,
    formValues,
    fields
  );

  const requestDirector: RequestDirector<NewBulkEditResourceObject> =
    new RequestDirector<NewBulkEditResourceObject>(requestBuilder);

  return requestDirector.createRequest();
}

/**
 * Marshall form values to a request that can be passed to the API to overwrite
 * an existing transaction.
 *
 * @param {FormValues} formValues
 *   Form values representing the transaction that should be overwritten.
 * @param {Fields} fields
 *   Formatted field configuration data.
 * @param {TransactionID} id
 *   The ID of the transaction to overwrite.
 * @return {Request<NewBulkEditResourceObject>}
 *   A marshalled request which can be passed to the API to overwrite an
 *   existing transaction.
 */
function marshallOverwriteExistingTransactionRequest(
  formValues: FormValues,
  fields: Fields,
  id: TransactionID
): Request<BulkEditResourceObject> {
  const requestBuilder = new UpdateExistingTransactionRequestBuilder(
    ENTITY_TYPE_BULK_EDIT_TRANSACTION,
    formValues,
    fields,
    id
  );

  const requestDirector: RequestDirector<BulkEditResourceObject> =
    new RequestDirector<BulkEditResourceObject>(requestBuilder);

  return requestDirector.createRequest();
}

/**
 * Unmarshall transaction data from the API to a set of form values that Ant can
 * use to populate form items.
 *
 * @param {Response<BulkEditResourceObject>} restoredTransaction
 *   A restored transaction from the API.
 * @param {Fields} fields
 *   Formatted field configuration data.
 * @return {FormValues}
 *   Form values that Ant should use to represent this saved transaction.
 */
function unmarshallRequest(
  restoredTransaction: Response<BulkEditResourceObject>,
  fields: Fields
): FormValues {
  const formValuesBuilder: RestoreTransactionFormValuesBuilder =
    new RestoreTransactionFormValuesBuilder(restoredTransaction, fields);

  const formValuesDirector: FormValuesDirector = new FormValuesDirector(
    formValuesBuilder
  );

  return formValuesDirector.createFormValues();
}

/**
 * An async thunk which returns the time that the bulk edit with the given ID
 * was last saved, in this session or in any other, by this user or by any
 * other, according to the server.
 *
 * @return {Promise<TransactionTime>}
 *   A promise that, when resolved, will be the time that the bulk edit was
 *   last saved, according to the server, as a unixtime value including
 *   milliseconds.
 */
export const getTimeLastSaved = createAsyncThunk<
  TransactionTime,
  TransactionID,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/getTimeLastSavedStatus",
  async (id: TransactionID, thunkAPI): Promise<TransactionTime> => {
    const drupalAPI: DrupalAPI = thunkAPI.extra.getDrupalAPI();

    // redux-bees caches API calls, but cached data is hardly useful when we
    // need to accurately determine when the transaction was last saved. The
    // variable `this.#timesGetTransactionCalled` is therefore used to bust the
    // cache.
    //
    // redux-bees offers two _official_ solutions to this problem,[1][2] but I
    // (John K.) have not been able to make either work.[3] The first method
    // also requires the use of a class-based decorator, requiring that
    // functional components be wrapped in classes, which is a bit annoying.[4]
    //
    // [1] https://github.com/Inveniem/redux-bees/tree/1eb449af0d92b8c468f39ac6b418baf5b4e48ddc#forced-refetch
    // [2] https://github.com/Inveniem/redux-bees/tree/1eb449af0d92b8c468f39ac6b418baf5b4e48ddc#cache-invalidation
    // [3] https://github.com/Inveniem/redux-bees/issues/1
    // [3] https://github.com/Inveniem/redux-bees/issues/2
    const transaction = await drupalAPI.getTransaction(
      thunkAPI.dispatch,
      thunkAPI.getState,
      {
        id,
        "cache-buster": timesGetTransactionCalled,
      }
    );

    timesGetTransactionCalled += 1;

    return new Date(transaction.body.data.attributes.changed).getTime();
  }
);

/**
 * An async thunk which saves a new transaction or overwrites an existing
 * transaction for later editing or submission.
 */
export const save = createAsyncThunk<
  SaveReport,
  {
    transactionID: TransactionID | null;
    formValues: FormValues;
  },
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/saveStatus",

  /**
   * @param {Object} payload
   *   The thunk payload.
   * @param {TransactionID | null} payload.transactionID
   *   The ID of a transaction to overwrite. If null, a new transaction will be
   *   created.
   * @param {FormValues} payload.formValues
   *   Form values of the transaction that should be saved.
   * @param {Object} thunkAPI
   *   An object containing all of the parameters that are normally passed to a
   *   Redux thunk function, as well as additional options. See the {@link
   *   https://redux-toolkit.js.org/api/createAsyncThunk#payloadcreator Redux
   *   documentation about `payloadCreator`} for more information.
   *
   * @return {Promise<SaveReport>}
   *   A promise that, when resolved, will contain the ID of the transaction and
   *   the time it was saved as a unixtime value including milliseconds.
   */
  async ({ transactionID, formValues }, thunkAPI): Promise<SaveReport> => {
    const drupalAPI: DrupalAPI = thunkAPI.extra.getDrupalAPI();
    let response;

    // Field data is needed to marshall this request. It's fetched elsewhere in
    // the application, but it may not have finished loading by this point. Keep
    // trying to get the data until it arrives.
    //
    // There are probably better solutions to this problem. For instance, we
    // could load field data in some top-level component like _BulkEditor.tsx_
    // and disable the save button until all field data is loaded successfully,
    // but that would take some refactoring and this works well enough for now.
    const fields: Fields = await getFetchableWithRetries<Fields>(
      (): Fetchable<Fields> => thunkAPI.getState().fields
    );

    if (transactionID) {
      const request: Request = marshallOverwriteExistingTransactionRequest(
        formValues,
        fields,
        transactionID
      );
      response = await drupalAPI.overwriteExistingTransaction(
        thunkAPI.dispatch,
        thunkAPI.getState,
        { transactionID },
        request
      );
    } else {
      const request: Request = marshallSaveNewTransactionRequest(
        formValues,
        fields
      );
      response = await drupalAPI.saveNewTransaction(
        thunkAPI.dispatch,
        thunkAPI.getState,
        undefined,
        request
      );
    }

    // Record the UUID that Drupal assigned to each edit.
    response.body.data.relationships.edits.data.forEach(
      (datum: { id: string }, index: number): void => {
        thunkAPI.dispatch(
          setPreviouslyUsedEditUUID({
            editName: index,
            value: datum.id,
          })
        );
      }
    );

    return {
      id: response.body.data.id,
      time: new Date(response.body.data.attributes.changed).getTime(),
    };
  }
);

/**
 * A thunk which populates the Redux store with state about a transaction that
 * was previously saved.
 */
export const restore = createAsyncThunk<
  FormValues,
  TransactionID,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/restoreStatus",

  /**
   * @param {TransactionID} transactionID
   *   The ID of the transaction to restore.
   * @param {Object} thunkAPI
   *   An object containing all of the parameters that are normally passed to a
   *   Redux thunk function, as well as additional options. See the {@link
   *   https://redux-toolkit.js.org/api/createAsyncThunk#payloadcreator Redux
   *   documentation about `payloadCreator`} for more information.
   *
   * @return {Promise<FormValues>}
   *   A promise that, when resolved, will be the form values of the transaction that was
   *   restored.
   */
  async (transactionID: TransactionID, thunkAPI): Promise<FormValues> => {
    const drupalAPI: DrupalAPI = thunkAPI.extra.getDrupalAPI();

    const transaction = await drupalAPI.getTransaction(
      thunkAPI.dispatch,
      thunkAPI.getState,
      {
        id: transactionID,
        include: "edits",
      }
    );

    // Field data is needed to unmarshall this request. It's fetched elsewhere
    // in the application, but it may not have finished loading by this point.
    // Keep trying to get the data until it arrives.
    //
    // There are probably better solutions to this problem. For instance, we
    // could load field data in some top-level component like _BulkEditor.tsx_
    // and not attempt a restoration until all field data is loaded
    // successfully, but that would take some refactoring and this works well
    // enough for now.
    const fields: Fields = await getFetchableWithRetries<Fields>(
      (): Fetchable<Fields> => thunkAPI.getState().fields
    );

    return unmarshallRequest(transaction.body, fields);
  }
);

/**
 * An async thunk which restores this bulk edit from the server if it has
 * previously been saved.
 */
export const attemptRestore = createAsyncThunk<
  void,
  FormInstance,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/getTimeLastSavedStatus",

  /**
   * @param {FormInstance} form
   *   The form instance, as returned by Ant's `Form.useForm()` method, that is
   *   being used for the bulk edit form.
   * @param {Object} thunkAPI
   *   An object containing all of the parameters that are normally passed to a
   *   Redux thunk function, as well as additional options. See the {@link
   *   https://redux-toolkit.js.org/api/createAsyncThunk#payloadcreator Redux
   *   documentation about `payloadCreator`} for more information.
   */
  async (form: FormInstance, thunkAPI): Promise<void> => {
    const { transactionID }: State = thunkAPI.getState().persistence;
    if (transactionID !== null) {
      try {
        const restoredFormValues: FormValues = await thunkAPI
          .dispatch<any>(restore(transactionID))
          .unwrap();

        // The documentation for `form.setFieldsValue` is somewhat sparse, but
        // it should be passed an object where the keys are form names and the
        // values are form values.
        //
        // https://ant.design/components/form/v3#Form-control
        form.setFieldsValue(restoredFormValues);
      } catch (err) {
        // TODO (ARKNG-1062): Handle server errors.
        // dispatch().unwrap() will throw if there is a server error.
      }
    }
  }
);

/**
 * Redux slice which stores data related to saved transactions and which
 * provides methods for updating that data.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
export const persistenceSlice = createSlice({
  name: "persistence",
  initialState,
  reducers: {
    setEgocentricTimeLastSaved: (
      state: Draft<State>,
      action: PayloadAction<SetEgocentricTimeLastSavedAction>
    ): void => {
      state.egocentricTimeLastSaved = action.payload;
    },
    setTransactionID: (
      state: Draft<State>,
      action: PayloadAction<SetTransactionIDAction>
    ): void => {
      state.transactionID = action.payload;
    },
  },
});

/**
 * Redux action for setting or updating the transaction ID.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {SetTransactionIDAction} action
 *   An action containing the new transaction ID.
 * @type {function}
 */
export const { setTransactionID } = persistenceSlice.actions;

/**
 * Redux action for setting or updating the egocentric time last saved.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {SetEgocentricTimeLastSavedAction} action
 *   An action containing the new transaction ID.
 * @type {function}
 */
export const { setEgocentricTimeLastSaved } = persistenceSlice.actions;

/**
 * Redux selector for retrieving the transaction ID.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {TransactionID | null}
 *   The transaction ID, or null if the transaction does not have an ID.
 */
export const selectTransactionID = (state: RootState): TransactionID | null =>
  state.persistence.transactionID;

/**
 * Redux selector for retrieving the egocentric time last saved.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {TransactionID | null}
 *   The egocentric time last saved, or null if the transaction has never been
 *   saved.
 */
export const selectEgocentricTimeLastSaved = (
  state: RootState
): TransactionTime | null => state.persistence.egocentricTimeLastSaved;

/**
 * A thunk which fetches and sets the value of `egocentricTimeLastSaved` and
 * enables saving.
 *
 * See the {@link https://redux.js.org/usage/writing-logic-thunks Redux
 * documentation about thunks} for more information.
 *
 * @return {Promise<void>}
 *   A Promise which does include any value in its resolution.
 */
export const fetchEgocentricTimeLastSaved =
  () =>
  async (dispatch: AppDispatch, getState: () => RootState): Promise<void> => {
    const transactionID: TransactionID | null = selectTransactionID(getState());
    if (transactionID !== null) {
      try {
        const timeLastSaved: TransactionTime = await dispatch<any>(
          getTimeLastSaved(transactionID)
        ).unwrap();
        dispatch(setEgocentricTimeLastSaved(timeLastSaved));
      } catch (err) {
        // TODO (ARKNG-1062): Handle server errors.
        // dispatch().unwrap() will throw if there is a server error.
      }
    }

    // Saving is disabled by default because `egocentricTimeLastSaved` is
    // fetched asynchronously and we need to know its value before we can safely
    // save anything.
    //
    // Now that we have the value, we can enable saving.
    dispatch(setSaveEnabled(true));
  };

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default persistenceSlice.reducer;
