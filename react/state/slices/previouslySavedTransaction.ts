import { createSlice } from "@reduxjs/toolkit";
import { Fetchable, FetchStatus } from "@custom-types/state";

import { restore } from "./persistence";

import type { FormValues } from "@custom-types/form";
import type { RootState } from "../store";

const initialState: Fetchable<FormValues> = {
  status: FetchStatus.Idle,
  data: null,
};

/**
 * Redux slice which stores a previously saved transaction which has been
 * restored.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
export const previouslySavedTransactionSlice = createSlice({
  name: "previouslySavedTransaction",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(restore.pending, (state) => {
      if (state.status === FetchStatus.Idle) {
        state.status = FetchStatus.Pending;
      }
    });
    builder.addCase(restore.fulfilled, (state, action) => {
      if (state.status === FetchStatus.Pending) {
        state.status = FetchStatus.Fulfilled;
        state.data = action.payload;
      }
    });
    builder.addCase(restore.rejected, (state) => {
      if (state.status === FetchStatus.Pending) {
        state.status = FetchStatus.Rejected;
      }
    });
  },
});

/**
 * Redux selector for retrieving the previously saved transaction's loading
 * status and data, if any.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {Fetchable<FormValues>}
 *   The previously saved transaction's loading status and data, if any.
 */
export const selectPreviouslySavedTransaction = (
  state: RootState
): Fetchable<FormValues> => state.previouslySavedTransaction;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default previouslySavedTransactionSlice.reducer;
