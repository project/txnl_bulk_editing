import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Fetchable, FetchStatus } from "@custom-types/state";
import { fetchPaginatedAttributes } from "@utils/json-api";

import type DrupalAPI from "../../api";
import type { AppDispatch, RootState } from "../store";

export type FieldStorageConfig = {
  cardinality: number;
  field_name: string;
};

const initialState: Fetchable<FieldStorageConfig[]> = {
  status: FetchStatus.Idle,
  data: null,
};

/**
 * Redux thunk which fetches field storage config.
 */
export const fetchFieldStorageConfig = createAsyncThunk<
  FieldStorageConfig[],
  undefined,
  {
    dispatch: AppDispatch;
    state: RootState;
    extra: {
      getDrupalAPI: () => DrupalAPI;
    };
  }
>(
  "fields/fetchFieldStorageConfigStatus",
  async (arg: undefined, thunkAPI): Promise<FieldStorageConfig[]> => {
    const drupalAPI: DrupalAPI = thunkAPI.extra.getDrupalAPI();

    const requestArgs = {
      "filter[entity_type]": "node",
    };

    return fetchPaginatedAttributes<FieldStorageConfig>(
      thunkAPI.dispatch,
      thunkAPI.getState,
      drupalAPI.getFieldStorageConfig,
      requestArgs
    );
  }
);

/**
 * Redux slice which stores field storage config.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
const fieldStorageConfigSlice = createSlice({
  name: "fieldStorageConfig",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchFieldStorageConfig.pending, (state) => {
      if (state.status === FetchStatus.Idle) {
        state.status = FetchStatus.Pending;
      }
    });
    builder.addCase(fetchFieldStorageConfig.fulfilled, (state, action) => {
      if (state.status === FetchStatus.Pending) {
        state.status = FetchStatus.Fulfilled;
        state.data = action.payload;
      }
    });
    builder.addCase(fetchFieldStorageConfig.rejected, (state) => {
      state.status = FetchStatus.Rejected;
    });
  },
});

/**
 * Redux selector for retrieving field storage config loading status and data,
 * if any.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {Fetchable<FieldStorageConfig[]>}
 *   Field storage config loading status and data, if any.
 */
export const selectFieldStorageConfig = (
  state: RootState
): Fetchable<FieldStorageConfig[]> => state.fieldStorageConfig;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default fieldStorageConfigSlice.reducer;
