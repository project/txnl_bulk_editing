import { createSlice } from "@reduxjs/toolkit";

import type { PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../store";

// TODO (ARKNG-844): Expand this type definition once we begin showing real data
// from nodes.
export type Result = any;

type ResultsState = {
  results: Result[];
  filteredResults: Result[];
};

type SetResultsAction = Result[];
type SetFilteredResultsAction = Result[];

const initialState: ResultsState = {
  results: [],
  filteredResults: [],
};

/**
 * Redux slice which stores the data for all nodes and all filtered nodes and
 * which provides methods for updating that data.
 *
 * See the {@link
 * https://redux.js.org/tutorials/essentials/part-2-app-structure#redux-slices
 * Redux documentation about slices} for more information.
 */
export const resultsSlice = createSlice({
  name: "results",
  initialState,
  reducers: {
    setResults: (state, action: PayloadAction<SetResultsAction>): void => {
      state.results = action.payload;
    },
    setFilteredResults: (
      state,
      action: PayloadAction<SetFilteredResultsAction>
    ): void => {
      state.filteredResults = action.payload;
    },
  },
});

/**
 * Redux action for setting or updating the data for all nodes.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {SetResultsAction} action
 *   An action containing the data for all nodes.
 * @type {function}
 */
export const { setResults } = resultsSlice.actions;

/**
 * Redux action for setting or updating the data for all filtered nodes.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#action Redux
 * glossary entry for "Action"} for more information.
 *
 * @param {SetResultsAction} action
 *   An action containing the data for all filtered nodes.
 * @type {function}
 */
export const { setFilteredResults } = resultsSlice.actions;

/**
 * Redux selector for retrieving the data for all nodes.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {Result[]}
 *   The data for all nodes.
 */
export const selectResults = (state: RootState): Result[] =>
  state.results.results;

/**
 * Redux selector for retrieving the data for all nodes matched by the active
 * filter.
 *
 * See the {@link
 * https://redux.js.org/usage/deriving-data-selectors#basic-selector-concepts
 * Redux documentation about selectors} for more information.
 *
 * @param {RootState} state
 *   An object containing all state managed by the Redux store.
 *
 * @return {Result[]}
 *   The data for all nodes matched by the active filter.
 */
export const selectFilteredResults = (state: RootState): Result[] =>
  state.results.filteredResults;

/**
 * Redux reducer for combining individual actions into a single state.
 *
 * See the {@link
 * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
 * glossary entry for "Reducer"} for more information.
 */
export default resultsSlice.reducer;
