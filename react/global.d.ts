/**
 * Global variables for appeasing the TypeScript type checker.
 *
 * https://stackoverflow.com/a/68870599/715866
 */
declare global {
  var Drupal: {
    behaviors: {
      txnl_bulk_editing: {
        attach(context: DrupalContext, settings: DrupalSettings): void;
      };
    };
    t: Function;
  };
}

export {};
