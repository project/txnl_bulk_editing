import {
  buildApi as buildBeesAPI,
  get,
  getRequestResult,
  patch,
  post,
  // @ts-expect-error: No types are published for redux-bees.
  // https://github.com/Inveniem/redux-bees/issues/3
} from "@inveniem/redux-bees";

import type { Request } from "ts-json-api";
import type { AppDispatch, RootState } from "./state/store";

type API = {
  [key: string]: Function;
};

type BeesAPI = {
  [key: string]: any;
};

type Includes = string[];

type MethodConfig = {
  method: Function;
  path: string;
  defaultIncludes: Includes;
};

type Endpoints = {
  [key: string]: MethodConfig;
};

type Headers = {
  [key: string]: string | null;
};

export type GetStateFunction = () => RootState;

// Type that explicitly specifies nested data within the "bees" object. This
// type is used to satisfy the TypeScript type checker elsewhere in this file.
// It would probably not be needed if redux-bees provided its own types.
//
// https://github.com/Inveniem/redux-bees/issues/3
interface RootStateWithBees extends RootState {
  bees: { entities: { [key: string]: string | Result } };
}

type Arguments = {
  [key: string]: string;
};

type ReadOperation = (
  dispatch: AppDispatch,
  getState: GetStateFunction,
  requestArgs: Arguments
) => Promise<Result>;

type WriteOperation = (
  dispatch: AppDispatch,
  getState: GetStateFunction,
  requestArgs: Arguments,
  payload: Request
) => Promise<Result>;

type Result = {
  body: {
    data: {
      attributes: {
        token: string;
      };
    };
  };
};

type User = {
  attributes: { token: string };
};

/**
 * The JSON API entity type for the logged-in user.
 */
const entityTypeCurrentUser: string = "current_user--current_user";

/**
 * The JSON API entity type for bulk edit transactions.
 *
 * @type {string}
 */
export const ENTITY_TYPE_BULK_EDIT_TRANSACTION =
  "bulk_edit_transaction--bulk_edit_transaction";

// TODO (ARKNG-1002): Do not assume a URI prefix of "/api/v2", as this cannot be
// safely assumed on other sites which use this module after it is released to
// the open-source community.
const endpoints: Endpoints = {
  getCurrentUserInfo: {
    method: get,
    path: "api/v2/jsonapi/current_user",
    defaultIncludes: [],
  },
  getFieldGroups: {
    method: get,
    path: "jsonapi/entity_form_display/entity_form_display",
    defaultIncludes: [],
  },
  getFields: {
    method: get,
    path: "jsonapi/field_config/field_config",
    defaultIncludes: [],
  },
  getFieldStorageConfig: {
    method: get,
    path: "jsonapi/field_storage_config/field_storage_config",
    defaultIncludes: [],
  },
  getAllowedValues: {
    method: get,
    path: "jsonapi/field_values/:entityType/:bundle/:fieldName",
    defaultIncludes: [],
  },
  getTransaction: {
    method: get,
    path: "jsonapi/bulk_edit_transaction/:id",
    defaultIncludes: [],
  },
  saveNewTransaction: {
    method: post,
    path: "jsonapi/bulk_edit_transaction",
    defaultIncludes: [],
  },
  overwriteExistingTransaction: {
    method: patch,
    path: "jsonapi/bulk_edit_transaction/:transactionID",
    defaultIncludes: [],
  },
};

/**
 * A Redux-aware client for communicating with Drupal via JSON:API.
 *
 * This client wraps a Redux Bees API with additional logic that provides
 * caching and automatic handling of CSRF tokens.
 */
export default class DrupalAPI implements API {
  [key: string]: Function;

  #requestHeaders: Headers;

  #beesAPI: BeesAPI;

  /**
   * Initializes a new instance of the Drupal API for the given base URL.
   *
   * @param {string} baseURL
   *   The base URL for the Drupal deployment.
   */
  constructor(baseURL: string) {
    this.#requestHeaders = {};
    this.#beesAPI = this.#buildAPI(baseURL);
    this.#buildOperationMethods();
  }

  /**
   * Builds a Drupal API interface for the given base URL and header context.
   *
   * @param {string} baseURL
   *   The base URL for the Drupal deployment.
   *
   * @return {BeesAPI}
   *   A redux-bees API as returned by its buildApi function, with the specified
   *   `baseUrl` and necessary headers configured.
   */
  #buildAPI(baseURL: string): BeesAPI {
    const config = {
      baseUrl: baseURL,

      // configureHeaders() is used by Redux Bees.
      configureHeaders: (headers: Headers) => ({
        ...headers,
        ...this.#requestHeaders,
      }),
    };

    return buildBeesAPI(endpoints, config);
  }

  /**
   * Builds methods in this instance to wrap the methods of the Redux Bees API.
   *
   * Builds a method to wrap each operation defined in the Redux Bees API
   * object. Write operations automatically fetch and maintain a CSRF token for
   * the requests.
   */
  #buildOperationMethods(): void {
    Object.entries(endpoints).forEach(
      ([endpointMethodName, endpointMethodConfig]: [string, MethodConfig]) => {
        const apiMethod: Function = this.#beesAPI[endpointMethodName];
        const { defaultIncludes } = endpointMethodConfig;

        if (endpointMethodConfig.method === get) {
          this[endpointMethodName] = this.#buildReadOperation(
            apiMethod,
            defaultIncludes
          );
        } else {
          this[endpointMethodName] = this.#buildWriteOperation(
            apiMethod,
            defaultIncludes
          );
        }
      }
    );
  }

  /**
   * Builds an operation method that performs a read-only request.
   *
   * @param {Function} apiMethod
   *   The Redux Bees method from the Bees API object that the operation is
   *   being built for.
   * @param {Includes} defaultIncludes
   *   The default list of relationships to include in the resulting response
   *   data.
   *
   * @return {ReadOperation}
   *   A function that, when invoked, produces a promise to perform the
   *   corresponding operation.
   */
  #buildReadOperation(
    apiMethod: Function,
    defaultIncludes: Includes
  ): ReadOperation {
    const outerThis = this;

    /**
     * Invokes this operation using the provided Redux state and arguments.
     *
     * If the request was already performed in the current session, the
     * previously-cached response is returned.
     *
     * @param {AppDispatch} dispatch
     *   The callback from Redux to dispatch the API call as a Redux action.
     * @param {GetStateFunction} getState
     *   The callback from Redux to get the state of the component.
     * @param {Arguments} requestArgs
     *   An object containing the named arguments passed in the request URL.
     *
     * @return {Promise<Result>}
     *   A promise object that, when resolved, will contain the response.
     */
    return function read(
      dispatch: AppDispatch,
      getState: GetStateFunction,
      requestArgs: Arguments
    ): Promise<Result> {
      const fullRequestArgs = outerThis.#addIncludesArg(
        requestArgs,
        defaultIncludes
      );

      const cachedResult = getRequestResult(getState(), apiMethod, [
        fullRequestArgs,
      ]);

      let loadingPromise;

      if (cachedResult === null) {
        loadingPromise = dispatch(apiMethod(fullRequestArgs)).then(
          (result: Result) => result
        );
      } else {
        // Use cached result
        loadingPromise = Promise.resolve(cachedResult);
      }

      return loadingPromise;
    };
  }

  /**
   * Builds an operation method that performs a write request.
   *
   * @param {Function} apiMethod
   *   The Redux Bees method from the Bees API object that the operation is
   *   being built for.
   * @param {Includes} defaultIncludes
   *   The default list of relationships to include in the resulting response
   *   data.
   *
   * @return {WriteOperation}
   *   A function that, when invoked, produces a promise to perform the
   *   corresponding operation.
   */
  #buildWriteOperation(
    apiMethod: Function,
    defaultIncludes: Includes
  ): WriteOperation {
    const outerThis = this;

    /**
     * Invokes this operation using the provided Redux state and arguments.
     *
     * A CSRF token is automatically requested from the backend if one is not
     * already available. It is automatically sent in the X-CSRF-Token request
     * header.
     *
     * @param {AppDispatch} dispatch
     *   The callback from Redux to dispatch the API call as a Redux action.
     * @param {GetStateFunction} getState
     *   The callback from Redux to get the state of the component.
     * @param {Arguments} requestArgs
     *   An object containing the named arguments passed in the request URL.
     * @param {Payload} payload
     *   An object containing the named arguments to pass in the request body.
     *
     * @return {Promise<Result>}
     *   A promise object that, when resolved, will contain the response. For
     *   responses with no data (e.g. DELETE, the promise result is NULL).
     */
    return function write(
      dispatch: AppDispatch,
      getState: GetStateFunction,
      requestArgs: Arguments = {},
      payload: Request | Record<string, never> = {}
    ): Promise<Result> {
      const fullRequestArgs = outerThis.#addIncludesArg(
        requestArgs,
        defaultIncludes
      );

      const writePromise = outerThis
        .#getCsrfToken(dispatch, getState)
        .then((csrfToken: string | null) => {
          outerThis.#requestHeaders["X-CSRF-Token"] = csrfToken;
        })
        .then(() =>
          dispatch(apiMethod(fullRequestArgs, payload)).then(
            (result: Result) => result
          )
        );

      return writePromise;
    };
  }

  /**
   * Return a full set of arguments which can be passed to an API method, with
   * includes added if they are non-empty.
   *
   * If no includes are provided, the arguments are returned unaltered.
   *
   * @private
   *
   * @param {Arguments} args
   *   An object containing the named arguments to pass to the method.
   * @param {Includes} includes
   *   The list of fields to include in the resulting response data.
   *
   * @return {Arguments}
   *   A full set of arguments which can be passed to an API method.
   */
  #addIncludesArg(args: Arguments, includes: Includes): Arguments {
    if (includes && includes.length > 0) {
      return { ...args, include: includes.join(",") };
    }
    return args;
  }

  /**
   * Gets or requests the CSRF token of the current user.
   *
   * @param {AppDispatch} dispatch
   *   The callback from Redux to dispatch the API call as a Redux action.
   * @param {GetStateFunction} getState
   *   The callback from Redux to get the state of the component, which must
   *   include the key for Redux Bees.
   *
   * @return {Promise<string | null>}
   *   A Promise that, when resolved, includes the CSRF token of the user; or,
   *   resolves to null if the user is missing a token for any reason.
   */
  #getCsrfToken(
    dispatch: AppDispatch,
    getState: GetStateFunction
  ): Promise<string | null> {
    return this.#getCurrentUser(dispatch, getState).then(
      (currentUser: User) => currentUser?.attributes?.token || null
    );
  }

  /**
   * Gets or requests information on the logged-in user.
   *
   * @param {AppDispatch} dispatch
   *   The callback from Redux to dispatch the API call as a Redux action.
   * @param {GetStateFunction} getState
   *   The callback from Redux to get the state of the component, which must
   *   include the key for Redux Bees.
   *
   * @return {Promise<User>}
   *   A Promise that, when resolved, includes a "current_user" entity for the
   *   logged-in user.
   */
  #getCurrentUser(
    dispatch: AppDispatch,
    getState: GetStateFunction
  ): Promise<User> {
    const reduxState = getState() as RootStateWithBees;
    let currentUserPromise: Promise<User>;
    let currentUser: User | null = null;

    if (reduxState?.bees?.entities[entityTypeCurrentUser]) {
      currentUser = Object.values(
        reduxState.bees.entities[entityTypeCurrentUser]
      ).find(() => true);
    }

    if (currentUser) {
      currentUserPromise = Promise.resolve(currentUser);
    } else {
      currentUserPromise = dispatch(this.#beesAPI.getCurrentUserInfo()).then(
        (result: Result) => result.body.data
      );
    }

    return currentUserPromise;
  }
}
