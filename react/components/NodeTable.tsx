import React, { FunctionComponent } from "react";
import { Table } from "antd";

import type { Result } from "../state/slices/results";

/**
 * Ant Design's Column component, which is used with the {@link
 * https://ant.design/components/table/ Table} component.
 */
const { Column } = Table;

interface Props {
  dataSource: Result[];
}

/**
 * Component for a table of information about nodes.
 *
 * TODO (ARKNG-1002): Do not make many assumptions about which data is available
 * or should be displayed here, as many of these assumptions will not hold on
 * other sites which use this module after it is released to the open-source
 * community. For example, "Location" and "Number of media files" are specific
 * to Inveniem's use.
 *
 * TODO (ARKNG-1002): Use Drupal's standard verbiage, which includes, for
 * instance, "Updated" rather than "Edited".
 *
 * @return {ReactElement}
 *   A React element for information about nodes.
 */
const NodeTable: FunctionComponent<Props> = ({ dataSource }: Props) => (
  <Table
    className="bulk-editor-node-table"
    dataSource={dataSource}
    pagination={{
      hideOnSinglePage: true,
      pageSize: 50,
      position: ["bottomCenter"],
      showQuickJumper: true,
    }}
  >
    <Column title={Drupal.t("Title")} dataIndex="title" />
    <Column title={Drupal.t("Created")} dataIndex="created" />
    <Column title={Drupal.t("Edited")} dataIndex="edited" />
    <Column title={Drupal.t("Description")} dataIndex="description" />
    <Column title={Drupal.t("Location")} dataIndex="location" />
    <Column title={Drupal.t("Number of media files")} dataIndex="media_count" />
  </Table>
);

export default NodeTable;
