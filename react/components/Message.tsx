import React, { FunctionComponent } from "react";

import "./Message.css";

interface Props {
  title: string;
  description?: string | null;
}

/**
 * Component for a message, with title and optional description, that can be
 * passed to Ant's message() function to convey information to the user.
 *
 * @return {ReactElement}
 *   A React element for a message that can be displayed to the user.
 */
const Message: FunctionComponent<Props> = ({ title, description }: Props) => {
  Message.defaultProps = {
    description: null,
  };

  return (
    <>
      <span className="bulk-editor-message__title">{title}</span>
      {description !== null && (
        <span className="bulk-editor-message__description">{description}</span>
      )}
    </>
  );
};

export default Message;
