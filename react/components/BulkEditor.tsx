import React, {
  useCallback,
  useContext,
  useEffect,
  FunctionComponent,
} from "react";
import { Collapse, Form, message } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";

import ConfigurationContext, {
  Configuration,
} from "../contexts/configurationContext";
import FormContext from "../contexts/formContext";
import { useAppDispatch, useAppSelector } from "../hooks/state";
import {
  attemptRestore,
  fetchEgocentricTimeLastSaved,
  getTimeLastSaved,
  save,
  selectEgocentricTimeLastSaved,
  selectTransactionID,
  setEgocentricTimeLastSaved,
  setTransactionID,
  SaveReport,
  TransactionID,
  TransactionTime,
} from "../state/slices/persistence";

import SelectStage from "./stages/select/SelectStage";
import EditStage from "./stages/edit/EditStage";
import PreviewStage from "./stages/preview/PreviewStage";
import ApplyStage from "./stages/apply/ApplyStage";
import Message from "./Message";
import "./BulkEditor.css";

import type { FormValues } from "@custom-types/form";

/**
 * Ant Design's Panel component, which is used with the {@link
 * https://ant.design/components/collapse/ Collapse} component.
 */
const { Panel } = Collapse;

// Only show one message at a time. This makes certain aspects of the
// implementation easier (e.g., we do not need to use `message.destroy()` to
// hide a message that becomes obsolete) and it reduces the risk of causing
// confusion by showing two conflicting messages (e.g., "Saved" and "Not
// Saved"), one new and one old, on the screen at the same time.
message.config({
  maxCount: 1,
});

const formValidationTemplates = {
  // This looks strange, but it is correct.
  //
  // This is not actually a template literal. It is rather a string which
  // happens to use the same syntax. We need to pass it to Ant for Ant to
  // evaluate it.
  //
  // eslint-disable-next-line no-template-curly-in-string
  required: '"${label}" is required.',
};

/**
 * Component for the bulk edit form.
 *
 * @return {ReactElement}
 *   A React element for the bulk edit form.
 */
const BulkEditor: FunctionComponent = () => {
  const { editPagePath }: Configuration = useContext(ConfigurationContext);

  /**
   * Redux's dispatch function.
   *
   * See the {@link
   * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
   * glossary entry for "Dispatching Function"} for more information.
   */
  const dispatch = useAppDispatch();

  const [form] = Form.useForm();

  const transactionID: TransactionID | null =
    useAppSelector(selectTransactionID);
  const egocentricTimeLastSaved: TransactionTime | null = useAppSelector(
    selectEgocentricTimeLastSaved
  );

  // Restore this transaction if it was previously saved.
  //
  // Store the value of `egocentricTimeLastSaved` in the store and enable the
  // save button once it has been set.
  //
  // The second argument to `useEffect` is intentionally an empty array. This
  // ensures that the effect runs exactly once, when this component renders for
  // the first time.
  useEffect(() => {
    dispatch<any>(attemptRestore(form));
    dispatch<any>(fetchEgocentricTimeLastSaved());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  /**
   * Save the bulk edit and show a message explaining that it has been saved.
   */
  const handleSave = useCallback(
    async (formValues: FormValues): Promise<void> => {
      message.loading({ content: <Message title="Saving..." /> });

      try {
        const saveReport: SaveReport = await dispatch<any>(
          save({
            transactionID,
            formValues,
          })
        ).unwrap();

        dispatch(setEgocentricTimeLastSaved(saveReport.time));

        // If this is the first time the bulk edit has been saved, record its
        // newly-assigned ID and add it to the URL.
        if (transactionID === null) {
          dispatch(setTransactionID(saveReport.id));
          window.history.replaceState(
            null,
            "",
            `${editPagePath}${saveReport.id}`
          );
        }

        message.success({
          content: (
            <Message
              title={Drupal.t("Saved")}
              description={Drupal.t(
                "Bookmark this page to return to this bulk edit later."
              )}
            />
          ),
          duration: 10,
        });
      } catch (err) {
        // TODO (ARKNG-1062): Handle server errors.
        // dispatch().unwrap() will throw if there is a server error.
      }
    },
    [dispatch, editPagePath, transactionID]
  );

  /**
   * Handle an attempt to save this bulk edit when it cannot be saved due to a
   * conflict. Show an error message explaining why the bulk edit cannot be
   * saved.
   */
  const handleConflict = useCallback((): void => {
    message.error({
      content: (
        <Message
          title={Drupal.t("Not saved")}
          description={
            // Tell Prettier to ignore this node in the AST because it indents
            // this multi-line string strangely.
            //
            // prettier-ignore
            Drupal.t(
              "These changes cannot be saved because this bulk edit was " +
              "saved elsewhere while you were working on it. Once you are " +
              "sure that this bulk edit is not being modified elsewhere, " +
              "consider opening a new tab with the latest copy of this bulk " +
              "edit and entering your changes again."
            )
          }
        />
      ),
      duration: 0, // Never hide this message.
    });
  }, []);

  /**
   * Handle the case where the "Submit" button is clicked and all required form
   * fields have been provided. Save this bulk edit if it can be saved. If it
   * cannot be saved, show an error message.
   */
  const handleValidationSuccess = useCallback(
    async (formValues: FormValues): Promise<void> => {
      // Check if the bulk edit has an ID. If it does not, then it has not been
      // saved before. It is therefore safe to save it now. Doing so cannot
      // possibly overwrite an earlier version of this bulk edit.
      //
      // If this bulk edit does have an ID, then we should only save it if doing
      // so would not overwrite an earlier version of this bulk edit.
      //
      // Note that `getTimeLastSaved`, which makes an HTTP request, is only called
      // if strictly necessary, even though it complicates the branching logic
      // somewhat, because HTTP requests are slow.
      if (transactionID === null) {
        handleSave(formValues);
      } else {
        // `actualTimeLastSaved` is the time that this bulk edit (i.e., the bulk
        // edit with this ID) was most recently saved _according to the server_.
        // It might be the time that this bulk edit was saved by another user or
        // in another tab. Therefore, it will not necessarily agree with
        // `egocentricTimeLastSaved`.
        try {
          const actualTimeLastSaved: TransactionTime = await dispatch<any>(
            getTimeLastSaved(transactionID)
          ).unwrap();

          if (egocentricTimeLastSaved === actualTimeLastSaved) {
            handleSave(formValues);
          } else {
            handleConflict();
          }
        } catch (err) {
          // TODO (ARKNG-1062): Handle server errors.
          // dispatch().unwrap() will throw if there is a server error.
        }
      }
    },
    [
      dispatch,
      egocentricTimeLastSaved,
      handleConflict,
      handleSave,
      transactionID,
    ]
  );

  const handleValidationFailure = useCallback((): void => {
    message.error({
      content: (
        <Message
          title={Drupal.t("Not saved")}
          description={
            // Tell Prettier to ignore this node in the AST because it indents
            // this multi-line string strangely.
            //
            // prettier-ignore
            Drupal.t(
              "These changes cannot be saved because some fields require " +
              "your attention."
            )
          }
        />
      ),
      duration: 10,
    });
  }, []);

  return (
    <FormContext.Provider value={form}>
      <main className="bulk-editor">
        <Form
          form={form}
          name="bulk-editor"
          layout="vertical"
          validateMessages={formValidationTemplates}
          onFinish={handleValidationSuccess}
          onFinishFailed={handleValidationFailure}
        >
          <Collapse
            defaultActiveKey={["filter", "edit", "preview", "apply"]}
            expandIcon={({ isActive }) => (
              <CaretRightOutlined rotate={isActive ? 90 : 0} />
            )}
            bordered={false}
          >
            <Panel key="filter" header={Drupal.t("Select")}>
              <SelectStage />
            </Panel>
            <Panel key="edit" header={Drupal.t("Edit")}>
              <EditStage />
            </Panel>
            <Panel key="preview" header={Drupal.t("Preview")}>
              <PreviewStage />
            </Panel>
            <Panel key="apply" header={Drupal.t("Apply")}>
              <ApplyStage />
            </Panel>
          </Collapse>
        </Form>
      </main>
    </FormContext.Provider>
  );
};

export default BulkEditor;
