import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { Fetchable, FetchStatus } from "@custom-types/state";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../contexts/formListFieldDataContext";
import FormContext, { FormInstance } from "../../../../contexts/formContext";
import EditFieldContext from "../../../../contexts/editFieldContext";
import { useAppDispatch, useAppSelector } from "../../../../hooks/state";
import { selectFields, Field, Fields } from "../../../../state/slices/fields";
import {
  setEditRestored,
  selectEditsState,
  EditsState,
} from "../../../../state/slices/formStatus";
import { selectPreviouslySavedTransaction } from "../../../../state/slices/previouslySavedTransaction";

import FieldSelector from "./sections/FieldSelector";
import MethodSelector from "./sections/MethodSelector";
import EditInput from "./sections/EditInput";
import PreviouslyUsedUUID from "./sections/PreviouslyUsedUUID";
import RemoveButton from "./sections/RemoveButton";
import "./EditForm.css";

import type { RadioChangeEvent } from "antd/lib/radio";
import type { Edit } from "@custom-types/fields";
import type { FormValues } from "@custom-types/form";

type Props = {
  onRemoveButtonClick: () => void;
};

/**
 * Component for a form which allows the user to specify a single edit. More
 * than one of these forms may be present on the page at a single time.
 *
 * @return {ReactElement}
 *   A React element for a form which allows the user to specify a single edit.
 */
const EditForm: FunctionComponent<Props> = ({ onRemoveButtonClick }: Props) => {
  const form: FormInstance = useContext(FormContext);
  const { name: editName }: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  /**
   * Redux's dispatch function.
   *
   * See the {@link
   * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
   * glossary entry for "Dispatching Function"} for more information.
   */
  const dispatch = useAppDispatch();

  const fields: Fetchable<Fields> = useAppSelector(selectFields);
  const previouslySavedTransaction: Fetchable<FormValues> = useAppSelector(
    selectPreviouslySavedTransaction
  );
  const editsState: EditsState = useAppSelector(selectEditsState);
  const editRestored: boolean | undefined = editsState[editName]?.restored;

  /**
   * The currently-selected field for the edit and a function for updating it.
   *
   * If a transaction was restored, use the value from the restored transaction.
   */
  const [editField, setEditField] = useState<Field | null>(null);

  /**
   * The UUID previously used for this edit and a function for updating it.
   *
   * If a transaction was restored, use the value from the restored transaction.
   */
  const [previouslyUsedUUID, setPreviouslyUsedUUID] = useState<string | null>(
    null
  );

  /**
   * The currently-selected edit type and a function for updating it.
   *
   * If a transaction was restored, use the value from the restored transaction.
   */
  const [method, setMethod] = useState<string | null>(null);

  // If this transaction was previously saved, and we have not yet finished
  // restoring it, set some local state to the values from the restored
  // transaction.
  //
  // It's important that we record whether this edit has been restored, so that
  // we do not restore it a second time if the user deletes this edit form and
  // then creates a new one to replace it.
  //
  // None of this can be done above, when local state is assigned, because the
  // transaction is restored asynchronously and it is usually not restored by
  // the time local state is assigned.
  //
  // Note that we are using `useEffect`, which memoizes the function based on
  // the array of dependencies. As a result, the values will be set once the
  // saved transaction is fully loaded, but they will not be set again after
  // that unless any of the dependencies change.
  useEffect((): void => {
    if (editRestored) return;
    if (previouslySavedTransaction.status !== FetchStatus.Fulfilled) return;
    if (fields.status !== FetchStatus.Fulfilled) return;

    // Neither of these data objects should be null, since their requests have
    // been fulfilled, but TypeScript doesn't know that. This check appeases the
    // type-checker and adds some extra, likely redundant safety.
    if (fields.data === null || previouslySavedTransaction.data === null)
      return;

    const restoredEdit: Edit | undefined =
      previouslySavedTransaction.data.edits?.[editName];

    if (restoredEdit === undefined) return;

    setEditField(fields.data[restoredEdit.field]);
    setMethod(restoredEdit.method);

    // Not all edits have previously used UUIDs.
    const restoredUUID: string | undefined =
      restoredEdit["previously-used-uuid"];
    if (restoredUUID !== undefined) {
      setPreviouslyUsedUUID(restoredUUID);
    }

    dispatch(setEditRestored({ editName, value: true }));
  }, [dispatch, editName, editRestored, fields, previouslySavedTransaction]);

  // Update the value of the previously used UUID if it changes, for example
  // because the transaction was just saved.
  useEffect(() => {
    setPreviouslyUsedUUID(editsState[editName]?.previouslyUsedUUID || null);
  }, [editName, editsState]);

  /**
   * Handle the selection of a field by storing the field in local state,
   * clearing out the local state of the selected method, and clearing out all
   * fields.
   *
   * Clearing out the local state of the selected method makes the user choose
   * the method again, which is good because different types of fields have
   * different methods.
   */
  const handleFieldSelect = useCallback(
    (value: string): void => {
      const field: Field | undefined = fields.data?.[value];

      // `field` should never be undefined, since the field cannot be selected
      // if its data has not loaded, but TypeScript doesn't know that. This
      // check appeases the type-checker and adds some extra, likely redundant
      // safety.
      if (field !== undefined) {
        setEditField(field);
      }

      setMethod(null);

      form.resetFields([
        ["edits", editName, "method"],
        ["edits", editName, "taxonomy-multiple-add"],
        ["edits", editName, "taxonomy-multiple-remove"],
        ["edits", editName, "taxonomy-multiple-replace-contents"],
        ["edits", editName, "taxonomy-multiple-replace-values-old"],
        ["edits", editName, "taxonomy-multiple-replace-values-new"],
        ["edits", editName, "taxonomy-single-replace-contents"],
        ["edits", editName, "taxonomy-single-replace-values-old"],
        ["edits", editName, "taxonomy-single-replace-values-new"],
      ]);
    },
    [editName, fields, form]
  );

  /**
   * Handle a change to the edit type by storing the edit type in local state.
   */
  const handleMethodChange = useCallback((e: RadioChangeEvent): void => {
    setMethod(e.target.value);
  }, []);

  return (
    <section className="bulk-editor-edit-form">
      <RemoveButton onRemoveButtonClick={onRemoveButtonClick} />
      {previouslyUsedUUID !== null && (
        <PreviouslyUsedUUID value={previouslyUsedUUID} />
      )}
      <FieldSelector onSelect={handleFieldSelect} />
      {editField !== null && (
        <EditFieldContext.Provider value={editField}>
          <MethodSelector editMethod={method} onChange={handleMethodChange} />
          {method !== null && <EditInput editMethod={method} />}
        </EditFieldContext.Provider>
      )}
    </section>
  );
};

export default EditForm;
