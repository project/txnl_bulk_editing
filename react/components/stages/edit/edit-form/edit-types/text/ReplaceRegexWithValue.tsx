import React, { useContext, FunctionComponent } from "react";
import { Form, Input } from "antd";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../contexts/formListFieldDataContext";

/**
 * Component for form inputs that can be used to replace text matched by a regex
 * with a single text value in a field.
 *
 * @return {ReactElement}
 *   A React element for replacing text matched by a regex with a single text
 *   value in a field.
 */
const ReplaceRegexWithValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <>
      <Form.Item
        className="bulk-editor-form-item bulk-editor-edit-form__input-1"
        name={[formListFieldData.name, `text-regex-search`]}
        label={Drupal.t("Regular expression")}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        className="bulk-editor-form-item bulk-editor-edit-form__input-2"
        name={[formListFieldData.name, `text-regex-replace`]}
        label={Drupal.t("Replacement pattern")}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
    </>
  );
};

export default ReplaceRegexWithValue;
