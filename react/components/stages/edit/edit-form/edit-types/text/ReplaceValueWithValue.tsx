import React, { useContext, FunctionComponent } from "react";
import { Form, Input } from "antd";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../contexts/formListFieldDataContext";

/**
 * Component for form inputs that can be used to replace matched text in a
 * field.
 *
 * @return {ReactElement}
 *   A React element for replacing matched text in a field.
 */
const ReplaceValueWithValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <>
      <Form.Item
        className="bulk-editor-form-item bulk-editor-edit-form__input-1"
        name={[formListFieldData.name, `text-replace-value-old`]}
        label={Drupal.t("Old text")}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        className="bulk-editor-form-item bulk-editor-edit-form__input-2"
        name={[formListFieldData.name, `text-replace-value-new`]}
        label={Drupal.t("New text")}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
    </>
  );
};

export default ReplaceValueWithValue;
