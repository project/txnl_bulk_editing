import React, { useContext, FunctionComponent } from "react";
import { Form, Input } from "antd";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../contexts/formListFieldDataContext";

/**
 * Component for a form input that can be used to prepend text to the text in a
 * field.
 *
 * @return {ReactElement}
 *   A React element for prepending text to the text in a field.
 */
const PrependValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <Form.Item
      className="bulk-editor-form-item bulk-editor-edit-form__input-1"
      name={[formListFieldData.name, `text-prepend`]}
      label={Drupal.t("Text to add to beginning")}
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
  );
};

export default PrependValue;
