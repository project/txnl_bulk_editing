import React, { useContext, FunctionComponent } from "react";
import { Form, Input } from "antd";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../contexts/formListFieldDataContext";

/**
 * Component for a form input that can be used to append text to the text in a
 * field.
 *
 * @return {ReactElement}
 *   A React element for appending text to the text in a field.
 */
const AppendValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <Form.Item
      className="bulk-editor-form-item bulk-editor-edit-form__input-1"
      name={[formListFieldData.name, `text-append`]}
      label={Drupal.t("Text to add to end")}
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
  );
};

export default AppendValue;
