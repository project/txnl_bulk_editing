import React, { useContext, FunctionComponent } from "react";
import { Form, Input } from "antd";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../contexts/formListFieldDataContext";

/**
 * Component for a form input that can be used to replace all text in a field.
 *
 * @return {ReactElement}
 *   A React element for replacing all text in a field.
 */
const ReplaceContentsWithValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <Form.Item
      className="bulk-editor-form-item bulk-editor-edit-form__input-1"
      name={[formListFieldData.name, `text-replace-contents`]}
      label={Drupal.t("Replacement text")}
      rules={[{ required: true }]}
    >
      <Input />
    </Form.Item>
  );
};

export default ReplaceContentsWithValue;
