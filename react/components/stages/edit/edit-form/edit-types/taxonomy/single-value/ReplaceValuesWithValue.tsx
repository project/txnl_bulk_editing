import React, { FunctionComponent, useContext } from "react";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../../contexts/formListFieldDataContext";
import TermSelector from "../../../input-types/TermSelector";

/**
 * Component for form inputs that can be used to replace one or more terms with
 * a single term in a single-value taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for replacing one or more terms with a single term in a
 *   single-value taxonomy field.
 */
const ReplaceValuesWithValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <>
      <TermSelector
        multiple
        formItemProps={{
          className: "bulk-editor-form-item bulk-editor-edit-form__input-1",
          label: Drupal.t("Old value(s)"),
          name: [formListFieldData.name, `taxonomy-single-replace-matches-old`],
          rules: [{ required: true }],

          // Tell Prettier to ignore this node in the AST because it indents
          // this multi-line string strangely.
          //
          // prettier-ignore
          extra: Drupal.t(
            "One or more values that should be replaced by a single new " +
            "value in all matched records."
          ),
        }}
      />
      <TermSelector
        formItemProps={{
          className: "bulk-editor-form-item bulk-editor-edit-form__input-2",
          label: Drupal.t("New value"),
          name: [formListFieldData.name, `taxonomy-single-replace-matches-new`],
          rules: [{ required: true }],

          // Tell Prettier to ignore this node in the AST because it indents
          // this multi-line string strangely.
          //
          // prettier-ignore
          extra: Drupal.t(
            "The single value that should replace the old value(s) in all " +
            "matched records."
          ),
        }}
      />
    </>
  );
};

export default ReplaceValuesWithValue;
