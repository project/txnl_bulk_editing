import React, { FunctionComponent, useContext } from "react";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../../contexts/formListFieldDataContext";
import TermSelector from "../../../input-types/TermSelector";

/**
 * Component for a form input that can be used to replace all terms with a
 * single term in a single-value taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for replacing all terms with a single term in a
 *   single-value taxonomy field.
 */
const ReplaceContentsWithValue: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <TermSelector
      formItemProps={{
        label: Drupal.t("New value"),
        name: [formListFieldData.name, `taxonomy-single-replace-contents`],
        rules: [{ required: true }],

        // Tell Prettier to ignore this node in the AST because it indents this
        // multi-line string strangely.
        //
        // prettier-ignore
        extra: Drupal.t(
          "The single value that should replace the contents of the field in " +
          "all matched records."
        ),
      }}
    />
  );
};

export default ReplaceContentsWithValue;
