import React, { FunctionComponent, useContext } from "react";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../../contexts/formListFieldDataContext";
import TermSelector from "../../../input-types/TermSelector";

/**
 * Component for a form input that can be used to remove terms from a
 * multiple-value taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for removing terms from a multiple-value taxonomy field.
 */
const RemoveValues: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <TermSelector
      multiple
      formItemProps={{
        label: Drupal.t("Value(s) to remove"),
        name: [formListFieldData.name, `taxonomy-multiple-remove`],
        rules: [{ required: true }],

        // Tell Prettier to ignore this node in the AST because it indents this
        // multi-line string strangely.
        //
        // prettier-ignore
        extra: Drupal.t(
          "One or more values that should be removed from the field in all " +
          "matched records."
        ),
      }}
    />
  );
};

export default RemoveValues;
