import React, { FunctionComponent, useContext } from "react";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../../contexts/formListFieldDataContext";
import TermSelector from "../../../input-types/TermSelector";

/**
 * Component for a form input that can be used to replace all terms with one or
 * more terms in a multiple-value taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for replacing all terms with one or more terms in a
 *   multiple-value taxonomy field.
 */
const ReplaceContentsWithValues: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <TermSelector
      limitToCardinality
      multiple
      formItemProps={{
        label: Drupal.t("New value(s)"),
        name: [formListFieldData.name, `taxonomy-multiple-replace-contents`],
        rules: [{ required: true }],

        // Tell Prettier to ignore this node in the AST because it indents this
        // multi-line string strangely.
        //
        // prettier-ignore
        extra: Drupal.t(
          "One or more values that should replace the contents of the field " +
          "in all matched records."
        ),
      }}
    />
  );
};

export default ReplaceContentsWithValues;
