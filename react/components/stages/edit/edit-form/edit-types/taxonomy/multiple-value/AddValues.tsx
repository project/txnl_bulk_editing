import React, { FunctionComponent, useContext } from "react";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../../contexts/formListFieldDataContext";
import TermSelector from "../../../input-types/TermSelector";

/**
 * Component for a form input that can be used to add terms to a multiple-value
 * taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for adding terms to a multiple-value taxonomy field.
 */
const AddValues: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <TermSelector
      limitToCardinality
      multiple
      formItemProps={{
        label: Drupal.t("Value(s) to add"),
        name: [formListFieldData.name, `taxonomy-multiple-add`],
        rules: [{ required: true }],

        // Tell Prettier to ignore this node in the AST because it indents this
        // multi-line string strangely.
        //
        // prettier-ignore
        extra: Drupal.t(
          "One or more values that should be added to the field in all " +
          "matched records."
        ),
      }}
    />
  );
};

export default AddValues;
