import React, { useContext, FunctionComponent } from "react";

import TermSelector from "../../../input-types/TermSelector";
import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../../../contexts/formListFieldDataContext";

const ReplaceValuesWithValues: FunctionComponent = () => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <>
      <TermSelector
        multiple
        formItemProps={{
          className: "bulk-editor-form-item bulk-editor-edit-form__input-1",
          label: Drupal.t("Old value(s)"),
          name: [
            formListFieldData.name,
            `taxonomy-multiple-replace-matches-old`,
          ],
          rules: [{ required: true }],

          // Tell Prettier to ignore this node in the AST because it indents
          // this multi-line string strangely.
          //
          // prettier-ignore
          extra: Drupal.t(
            "The value or combination of values that should be replaced in " +
            "matched records. A record must have all of these values in " +
            "order for the combination to be replaced. If the record has " +
            "other values in this field that do not appear in this list, " +
            "they will not be affected."
          ),
        }}
      />
      <TermSelector
        multiple
        limitToCardinality
        formItemProps={{
          className: "bulk-editor-form-item bulk-editor-edit-form__input-2",
          label: Drupal.t("New value(s)"),
          name: [
            formListFieldData.name,
            `taxonomy-multiple-replace-matches-new`,
          ],
          rules: [{ required: true }],

          // Tell Prettier to ignore this node in the AST because it indents
          // this multi-line string strangely.
          //
          // prettier-ignore
          extra: Drupal.t(
            "The value(s) that should replace the combination of old " +
            "value(s) in all matched records."
          ),
        }}
      />
    </>
  );
};

export default ReplaceValuesWithValues;
