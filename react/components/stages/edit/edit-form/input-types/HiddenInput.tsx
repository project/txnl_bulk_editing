import React, { FunctionComponent, useContext } from "react";
import { Form, Input } from "antd";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../contexts/formListFieldDataContext";

type Props = {
  /**
   * A unique identifier for this form input.
   */
  identifier: string;

  /**
   * The hidden value.
   */
  value: any;
};

/**
 * Component for a hidden form input that keeps track of any given value.
 *
 * @return {ReactElement}
 *   A React element for a hidden form input that keeps track of any given
 *   value.
 */
const HiddenInput: FunctionComponent<Props> = ({
  identifier,
  value,
}: Props) => {
  const { name: editName }: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  return (
    <Form.Item name={[editName, identifier]} initialValue={value} hidden>
      <Input type="hidden" />
    </Form.Item>
  );
};

export default React.memo(HiddenInput);
