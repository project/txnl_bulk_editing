import React, { useContext, useEffect, FunctionComponent } from "react";
import { Form, Select } from "antd";

import EditFieldContext, {
  Field,
} from "../../../../../contexts/editFieldContext";
import { useAppDispatch, useAppSelector } from "../../../../../hooks/state";
import {
  fetchAllowedValues,
  selectAllowedValuesByFieldName,
  Value,
} from "../../../../../state/slices/allowedValuesByFieldName";

type Props = {
  /**
   * Props that should be passed to Ant's `Form.Item` component.
   *
   * See {@link https://ant.design/components/form/#Form.Item Ant's
   * documentation on `Form.Item` props} for more information.
   */
  formItemProps: {
    className?: string;
    extra?: string;
    label: string;
    name: [number, string];
    rules?: Object[];
  };

  /**
   * Whether the input should limit the number of terms that can be selected
   * according to the cardinality of the field that it is associated with.
   */
  limitToCardinality?: boolean;

  /**
   * Whether the input should allow the selection of multiple terms.
   */
  multiple?: boolean;
};

/**
 * Ant Design's Option component, which is used with the {@link
 * https://ant.design/components/select/ Select} component.
 */
const { Option } = Select;

/**
 * Component for a form input that can be used to select one or more taxonomy
 * terms.
 *
 * @return {ReactElement}
 *   A React element for selecting one or more taxonomy terms.
 */
const TermSelector: FunctionComponent<Props> = ({
  formItemProps,
  limitToCardinality,
  multiple,
}: Props) => {
  TermSelector.defaultProps = {
    limitToCardinality: false,
    multiple: false,
  };

  const editField: Field = useContext(EditFieldContext);

  /**
   * Redux's dispatch function.
   *
   * See the {@link
   * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
   * glossary entry for "Dispatching Function"} for more information.
   */
  const dispatch = useAppDispatch();

  // Fetch the allowed values for this edit's field.
  useEffect(() => {
    dispatch<any>(fetchAllowedValues(editField.name));
  }, [dispatch, editField]);

  const allowedTerms: Value[] | null =
    useAppSelector(selectAllowedValuesByFieldName)[editField.name] || null;

  let rules: Object[] = [];
  if (formItemProps.rules !== undefined) {
    rules = [...formItemProps.rules];
  }

  // If we should limit the number of terms that can be selected to the
  // cardinality, and we can do so, set a new rule which enforces this, but do
  // not overwrite any rules which have already been provided.
  if (
    multiple &&
    limitToCardinality &&
    editField.cardinality !== null &&
    editField.cardinality > 1 // -1 indicates support for infinitely many terms.
  ) {
    rules.push({
      max: editField.cardinality,
      type: "array",
      message: Drupal.t(
        '"@fieldLabel" supports a maximum of @maxTerms values.',
        {
          "@fieldLabel": editField.label,
          "@maxTerms": editField.cardinality,
        }
      ),
    });
  }

  // NOTE: Ant validation will only work if the input component (e.g., `Select`)
  // is a direct descendant of a `<Form.Item>` component. The two cannot be
  // defined in separate files, even if a `<Form.Item>` in the parent wraps a
  // custom component which defines the input component at its root.
  //
  // That is why we need to define the `<Form.Item>` here, taking
  // `formItemProps` as a prop, even though it would be more convenient to
  // define it elsewhere.
  //
  // https://github.com/ant-design/ant-design/issues/29220
  return (
    <Form.Item {...formItemProps} rules={rules}>
      <Select
        showSearch
        loading={allowedTerms === null}
        mode={multiple ? "multiple" : undefined}
        optionFilterProp="children"
        filterOption={(searchTerm: string, option) => {
          if (option?.children === undefined) return false;
          return String(option.children)
            .toLowerCase()
            .includes(searchTerm.toLowerCase());
        }}
      >
        {allowedTerms !== null &&
          allowedTerms.map(({ id, label }: Value) => (
            <Option key={id} value={id}>
              {label}
            </Option>
          ))}
      </Select>
    </Form.Item>
  );
};

export default TermSelector;
