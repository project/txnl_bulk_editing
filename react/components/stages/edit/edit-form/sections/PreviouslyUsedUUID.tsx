import React, { FunctionComponent } from "react";

import HiddenInput from "../input-types/HiddenInput";

type Props = {
  /**
   * The UUID that Drupal assigned to this edit when it was first saved.
   */
  value: string;
};

/**
 * Component for a hidden form input that keeps track of the UUID that Drupal
 * assigned to this edit when it was first saved.
 *
 * @return {ReactElement}
 *   A React element for a hidden form input that keeps track of the previously
 *   used UUID of this edit.
 */
const PreviouslyUsedUUID: FunctionComponent<Props> = ({ value }: Props) => (
  <HiddenInput identifier="previously-used-uuid" value={value} />
);

export default React.memo(PreviouslyUsedUUID);
