import React, { useContext, useEffect, FunctionComponent } from "react";
import { Form, Select } from "antd";
import { FieldType } from "@custom-types/fields";
import { Fetchable, FetchStatus } from "@custom-types/state";

import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../contexts/formListFieldDataContext";
import { useAppDispatch, useAppSelector } from "../../../../../hooks/state";
import {
  fetchFields,
  selectFields,
  Fields,
} from "../../../../../state/slices/fields";
import {
  fetchFieldGroups,
  selectFieldGroups,
  FieldGroup,
  FieldGroups,
} from "../../../../../state/slices/fieldGroups";

type Props = {
  onSelect: (value: string) => void;
};

/**
 * Ant Design's Option and OptGroup components, which are used with the {@link
 * https://ant.design/components/select/ Select} component.
 */
const { Option, OptGroup } = Select;

/**
 * Component for a form item which allows the user to choose the field that an
 * edit should apply to.
 *
 * @return {ReactElement}
 *   A React element for a form item which allows the user to choose the field
 *   that an edit should apply to.
 */
const FieldSelector: FunctionComponent<Props> = ({ onSelect }: Props) => {
  const formListFieldData: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  const fields: Fetchable<Fields> = useAppSelector(selectFields);
  const fieldGroups: Fetchable<FieldGroups> = useAppSelector(selectFieldGroups);

  /**
   * Redux's dispatch function.
   *
   * See the {@link
   * https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux
   * glossary entry for "Dispatching Function"} for more information.
   */
  const dispatch = useAppDispatch();

  // Add information about field groups and fields to the Redux store.
  //
  // The second argument to `useEffect` is intentionally an empty array. This
  // ensures that the effect runs exactly once, when this component renders for
  // the first time.
  useEffect(() => {
    dispatch<any>(fetchFields());
    dispatch<any>(fetchFieldGroups());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // fieldGroups sorted by Drupal weight and passed through Object.entries().
  let sortedFieldGroupEntries: [string, FieldGroup][] = [];
  if (fieldGroups.data !== null && fields.status === FetchStatus.Fulfilled) {
    sortedFieldGroupEntries = Object.entries(fieldGroups.data).sort(
      ([, valueA]: [string, FieldGroup], [, valueB]: [string, FieldGroup]) =>
        valueA.weight - valueB.weight
    );
  }

  return (
    <Form.Item
      className="bulk-editor-form-item bulk-editor-edit-form__field"
      label={Drupal.t("Field")}
      name={[formListFieldData.name, "field"]}
      rules={[{ required: true }]}
    >
      <Select onSelect={onSelect}>
        {sortedFieldGroupEntries.map(
          ([fieldGroupName, fieldGroup]: [string, any]) => {
            // Do not display tabs themselves as options.
            if (fieldGroup.format_type === "tabs") return null;

            return (
              <OptGroup key={fieldGroupName} label={fieldGroup.label}>
                {fieldGroup.children.map((fieldName: string) => {
                  if (fields.data === null) return null;

                  const field = fields.data[fieldName];

                  if (field === undefined) return null;

                  const supported: boolean = field.type !== FieldType.Unknown;

                  return (
                    <Option
                      key={fieldName}
                      value={fieldName}
                      label={field.label}
                      title={
                        supported
                          ? field.description
                          : Drupal.t("Editing this field is not yet supported.")
                      }
                      disabled={!supported}
                    >
                      {field.label}
                    </Option>
                  );
                })}
              </OptGroup>
            );
          }
        )}
      </Select>
    </Form.Item>
  );
};

export default FieldSelector;
