import React, { useContext, FunctionComponent } from "react";
import { FieldType } from "@custom-types/fields";

import EditFieldContext from "../../../../../contexts/editFieldContext";
import TextReplaceContentsWithValue from "../edit-types/text/ReplaceContentsWithValue";
import TextValueWithValue from "../edit-types/text/ReplaceValueWithValue";
import TextPrependValue from "../edit-types/text/PrependValue";
import TextAppendValue from "../edit-types/text/AppendValue";
import TextReplaceRegexWithValue from "../edit-types/text/ReplaceRegexWithValue";
import SingleValueTaxonomyReplaceValuesWithValue from "../edit-types/taxonomy/single-value/ReplaceValuesWithValue";
import SingleValueTaxonomyReplaceContentsWithValue from "../edit-types/taxonomy/single-value/ReplaceContentsWithValue";
import MultipleValueTaxonomyAddValues from "../edit-types/taxonomy/multiple-value/AddValues";
import MultipleValueTaxonomyRemoveValues from "../edit-types/taxonomy/multiple-value/RemoveValues";
import MultipleValueTaxonomyReplaceValuesWithValues from "../edit-types/taxonomy/multiple-value/ReplaceValuesWithValues";
import MultipleValueTaxonomyReplaceContentsWithValues from "../edit-types/taxonomy/multiple-value/ReplaceContentsWithValues";
import { Field } from "../../../../../state/slices/fields";

type Props = {
  /**
   * The currently-selected method for this edit.
   */
  editMethod: string;
};

/**
 * Component for one or more form inputs that should be shown for a single-value
 * taxonomy field, based on the selected method.
 *
 * @return {ReactElement | null}
 *   A React element for form inputs for a single-value taxonomy field, or null
 *   if the selected method cannot be determined.
 */
const SingleValueTaxonomyInput = React.memo<Props>(
  function SingleValueTaxonomyInput({ editMethod }: Props) {
    switch (editMethod) {
      case "replace_contents":
        return <SingleValueTaxonomyReplaceContentsWithValue />;
      case "replace_matches":
        return <SingleValueTaxonomyReplaceValuesWithValue />;
      default:
        return null;
    }
  }
);

/**
 * Component for one or more form inputs that should be shown for a
 * multiple-value taxonomy field, based on the selected method.
 *
 * @return {ReactElement | null}
 *   A React element for form inputs for a multiple-value taxonomy field, or
 *   null if the selected method cannot be determined.
 */
const MultipleValueTaxonomyInput = React.memo<Props>(
  function MultipleValueTaxonomyInput({ editMethod }: Props) {
    switch (editMethod) {
      case "add_values":
        return <MultipleValueTaxonomyAddValues />;
      case "remove_values":
        return <MultipleValueTaxonomyRemoveValues />;
      case "replace_matches":
        return <MultipleValueTaxonomyReplaceValuesWithValues />;
      case "replace_contents":
        return <MultipleValueTaxonomyReplaceContentsWithValues />;
      default:
        return null;
    }
  }
);

/**
 * Component for one or more form inputs that should be shown for a text field,
 * based on the selected method.
 *
 * @return {ReactElement | null}
 *   A React element for form inputs for a text field, or null if the selected
 *   method cannot be determined.
 */
const TextInput = React.memo<Props>(function TextInput({ editMethod }: Props) {
  switch (editMethod) {
    case "text-replace-contents-with-value":
      return <TextReplaceContentsWithValue />;
    case "text-replace-value-with-value":
      return <TextValueWithValue />;
    case "text-prepend-value":
      return <TextPrependValue />;
    case "text-append-value":
      return <TextAppendValue />;
    case "text-replace-regex-with-value":
      return <TextReplaceRegexWithValue />;
    default:
      return null;
  }
});

/**
 * Component for one or more form inputs that should be shown for this edit,
 * based on the current method and the type of the current field.
 *
 * @return {ReactElement | null}
 *   A React element for one or more form inputs that should be shown for this
 *   edit, or null if the type of the current field cannot be determined.
 */
const EditInput: FunctionComponent<Props> = ({ editMethod }: Props) => {
  const editField: Field = useContext(EditFieldContext);

  switch (editField.type) {
    case FieldType.SingleValueTaxonomy:
      return <SingleValueTaxonomyInput editMethod={editMethod} />;
    case FieldType.MultipleValueTaxonomy:
      return <MultipleValueTaxonomyInput editMethod={editMethod} />;
    case FieldType.Text:
      return <TextInput editMethod={editMethod} />;
    default:
      return null;
  }
};

export default React.memo(EditInput);
