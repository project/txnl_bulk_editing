import React, { useContext, FunctionComponent } from "react";
import { Form, Radio } from "antd";
import { FieldType } from "@custom-types/fields";

import EditFieldContext from "../../../../../contexts/editFieldContext";
import FormListFieldDataContext, {
  FormListFieldData,
} from "../../../../../contexts/formListFieldDataContext";
import { Field } from "../../../../../state/slices/fields";

import type { RadioChangeEvent } from "antd/lib/radio";

type Props = {
  /**
   * The currently-selected method for this edit.
   */
  editMethod: string | null;

  /**
   * A callback to run when the method for this edit changes.
   */
  onChange: (value: RadioChangeEvent) => void;
};

/**
 * Component for radio buttons that should be shown for selecting an edit method
 * for a single-value taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for selecting an edit method for a single-value taxonomy
 *   field.
 */
const SingleValueTaxonomyRadioButtons = React.memo<{}>(
  function SingleValueTaxonomyButtons() {
    return (
      <>
        <Radio.Button value="replace_matches">
          {Drupal.t("Replace matched value(s)")}
        </Radio.Button>
        <Radio.Button value="replace_contents">
          {Drupal.t("Replace all values")}
        </Radio.Button>
      </>
    );
  }
);

/**
 * Component for radio buttons that should be shown for selecting an edit method
 * for a multiple-value taxonomy field.
 *
 * @return {ReactElement}
 *   A React element for selecting an edit method for a multiple-value taxonomy
 *   field.
 */
const MultipleValueTaxonomyRadioButtons = React.memo<{}>(
  function MultipleValueTaxonomyButtons() {
    return (
      <>
        <Radio.Button value="add_values">
          {Drupal.t("Add value(s)")}
        </Radio.Button>
        <Radio.Button value="remove_values">
          {Drupal.t("Remove value(s)")}
        </Radio.Button>
        <Radio.Button value="replace_matches">
          {Drupal.t("Replace matched value(s)")}
        </Radio.Button>
        <Radio.Button value="replace_contents">
          {Drupal.t("Replace all values")}
        </Radio.Button>
      </>
    );
  }
);

/**
 * Component for radio buttons that should be shown for selecting an edit method
 * for a text field.
 *
 * @return {ReactElement}
 *   A React element for selecting an edit method for a text field.
 */
const TextRadioButtons = React.memo<{}>(function TextButtons() {
  return (
    <>
      <Radio.Button value="text-replace-contents-with-value">
        {Drupal.t("Replace all text")}
      </Radio.Button>
      <Radio.Button value="text-replace-value-with-value">
        {Drupal.t("Replace matched text")}
      </Radio.Button>
      <Radio.Button value="text-prepend-value">
        {Drupal.t("Add text to beginning")}
      </Radio.Button>
      <Radio.Button value="text-append-value">
        {Drupal.t("Add text to end")}
      </Radio.Button>
      <Radio.Button value="text-replace-regex-with-value">
        {Drupal.t("Regular expression (advanced)")}
      </Radio.Button>
    </>
  );
});

/**
 * Component for radio buttons that should be shown for selecting an edit method
 * for the chosen field.
 *
 * @return {ReactElement | null}
 *   A React element for selecting an edit method for the chosen field, or null
 *   if the type of the field cannot be determined.
 */
const RadioButtons = React.memo<{}>(function RadioButtons() {
  const editField: Field = useContext(EditFieldContext);

  switch (editField.type) {
    case FieldType.SingleValueTaxonomy:
      return <SingleValueTaxonomyRadioButtons />;
    case FieldType.MultipleValueTaxonomy:
      return <MultipleValueTaxonomyRadioButtons />;
    case FieldType.Text:
      return <TextRadioButtons />;
    default:
      return null;
  }
});

/**
 * Component for a form item that should be provided for selecting an edit
 * method for the chosen field.
 *
 * @return {ReactElement | null}
 *   A React element for a form item that should be provided for selecting an
 *   edit method for the chosen field, or null if the type of the field is
 *   unknown.
 */
const MethodSelector: FunctionComponent<Props> = ({
  editMethod,
  onChange,
}: Props) => {
  const editField: Field = useContext(EditFieldContext);
  const { name: editName }: FormListFieldData = useContext(
    FormListFieldDataContext
  );

  if (editField.type === FieldType.Unknown) return null;

  return (
    <Form.Item
      className="bulk-editor-form-item bulk-editor-edit-form__edit-method"
      name={[editName, "method"]}
      label={Drupal.t("Method")}
      rules={[{ required: true }]}
    >
      <Radio.Group onChange={onChange} value={editMethod}>
        <RadioButtons />
      </Radio.Group>
    </Form.Item>
  );
};

export default React.memo(MethodSelector);
