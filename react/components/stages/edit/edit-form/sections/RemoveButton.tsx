import React, { FunctionComponent } from "react";
import { Button, Tooltip } from "antd";
import { CloseOutlined } from "@ant-design/icons";

type Props = {
  /**
   * A function which should be called when this button is clicked.
   */
  onRemoveButtonClick: () => void;
};

/**
 * React component for a button which can be used to remove an edit form.
 *
 * Show this button even if this is the only edit. We could omit this button
 * when there is only one edit, but sometimes, it's easier to remove the only
 * edit and start it from scratch than it is to manually clear its fields.
 *
 * @return {ReactElement}
 *   A React element for removing an edit form.
 */
const RemoveButton: FunctionComponent<Props> = ({
  onRemoveButtonClick,
}: Props) => (
  <Button
    className="bulk-editor-form-item bulk-editor-edit-form__remove"
    onClick={onRemoveButtonClick}
    aria-label={Drupal.t("Remove edit")}
    type="text"
    icon={
      <Tooltip title={Drupal.t("Remove edit")}>
        <CloseOutlined aria-hidden="true" />
      </Tooltip>
    }
  />
);

export default React.memo(RemoveButton);
