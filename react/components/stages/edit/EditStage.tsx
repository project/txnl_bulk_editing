import React, { FunctionComponent } from "react";
import { Button, Form } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import FormListFieldDataContext from "../../../contexts/formListFieldDataContext";

import EditForm from "./edit-form/EditForm";
import "./EditStage.css";

import type { FormListFieldData } from "antd/lib/form/FormList";

/**
 * Component for the "Edit" stage of the bulk edit form.
 *
 * @return {ReactElement}
 *   A React element for the "Edit" stage of the bulk edit form.
 */
const EditStage: FunctionComponent = () => {
  // An empty edit. This causes one empty edit form to appear when the page is
  // loaded.
  const initialValue: Object[] = [{}];

  return (
    <Form.List name="edits" initialValue={initialValue}>
      {(formListFieldDataArray: FormListFieldData[], { add, remove }) => (
        <>
          {formListFieldDataArray.map(
            (formListFieldData: FormListFieldData) => {
              return (
                <FormListFieldDataContext.Provider
                  key={formListFieldData.key}
                  value={formListFieldData}
                >
                  <EditForm
                    key={formListFieldData.key}
                    onRemoveButtonClick={() => remove(formListFieldData.name)}
                  />
                </FormListFieldDataContext.Provider>
              );
            }
          )}
          <Form.Item name="edits-add">
            <Button
              className="bulk-editor-stage__button--text bulk-editor-edit-stage__add-button"
              type="dashed"
              icon={<PlusOutlined />}
              onClick={add}
            >
              {Drupal.t("Add edit")}
            </Button>
          </Form.Item>
        </>
      )}
    </Form.List>
  );
};

export default EditStage;
