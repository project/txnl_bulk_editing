import React, { useState, useEffect, FunctionComponent } from "react";

import NodeTable from "../../NodeTable";
import {
  setResults,
  selectResults,
  Result,
} from "../../../state/slices/results";
import { useAppSelector, useAppDispatch } from "../../../hooks/state";

/**
 * Component which displays all nodes that match the filter that the user
 * provided.
 *
 * @return {ReactElement}
 *   A React Element which displays all nodes that match the filter.
 */
const ResultsComponent: FunctionComponent = () => {
  /**
   * Redux's dispatch function.
   *
   * See the {@link https://redux.js.org/understanding/thinking-in-redux/glossary#reducer Redux glossary entry for
   * "Dispatching Function"} for more information.
   */
  const dispatch = useAppDispatch();

  /**
   * The current array of data for all nodes from the Redux store.
   */
  const results: Result[] = useAppSelector<Result[]>(selectResults);

  // TODO (ARKNG-844): Remove this ESLint disable once we begin showing real
  // data from nodes.
  //
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  /**
   * Load and format data for all nodes.
   */
  useEffect(() => {
    // TODO (ARKNG-844): Remove this code once we begin showing real data from
    // nodes.
    setIsLoaded(true);
    dispatch(
      setResults([
        {
          key: "TBD",
          title: "TBD",
          created: "TBD",
          edited: "TBD",
          description: "TBD",
          location: "TBD",
          media_count: "TBD",
        },
      ])
    );

    // TODO (ARKNG-844): Uncomment and update this code once we begin showing
    // real data from nodes.
    //
    // fetch("TBD")
    //   .then((res) => res.json())
    //   .then(
    //     (res) => {
    //       setIsLoaded(true);
    //       dispatch(setResults(formatResults(res)));
    //     },
    //     (err) => {
    //       setIsLoaded(true);
    //       setError(err);
    //     }
    //   );
  }, [dispatch]);

  if (error) {
    // @ts-expect-error: Ignore this error until we start passing around real errors.
    return <div>Error: {error.message}</div>;
  }

  if (!isLoaded) {
    return <div>Loading...</div>;
  }

  if (results.length) {
    return <NodeTable dataSource={results} />;
  }

  return <div>Error: No results could be loaded</div>;
};

export default ResultsComponent;
