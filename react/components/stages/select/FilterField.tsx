import React, { FunctionComponent } from "react";
import { Form, Input } from "antd";

/**
 * Component for an input field that can be used to specify a filter that should
 * be applied to all nodes.
 *
 * @return {ReactElement}
 *   A React element for an input field that can be used to specify a filter.
 */
const FilterField: FunctionComponent = () => (
  <Form.Item name="filter" label={Drupal.t("Filter")}>
    <Input id="bulk-editor-form-item__control--filter" allowClear />
  </Form.Item>
);

export default FilterField;
