import React, { FunctionComponent } from "react";

import FilterField from "./FilterField";
import Results from "./Results";

/**
 * Component for the "Filter" stage of the bulk edit form.
 *
 * @return {ReactElement}
 *   A React element for the "Filter" stage of the bulk edit form.
 */
const SelectStage: FunctionComponent = () => (
  <>
    <p>
      {
        // Tell Prettier to ignore this node in the AST because it indents this
        // multi-line string strangely.
        //
        // prettier-ignore
        Drupal.t(
          "Provide an optional filter to select a limited set of records " +
          "that should be affected by this edit."
        )
      }
    </p>
    <FilterField />
    <Results />
  </>
);

export default SelectStage;
