import React, { FunctionComponent } from "react";

import NodeTable from "../../NodeTable";

/**
 * Component for the "Preview" stage of the bulk edit form.
 *
 * @return {ReactElement}
 *   A React element for the "Preview" stage of the bulk edit form.
 */
const PreviewStage: FunctionComponent = () => (
  // TODO (ARKNG-844): Modify this once we begin showing real data from nodes.
  <NodeTable
    dataSource={[
      {
        key: "TBD",
        title: "TBD",
        created: "TBD",
        edited: "TBD",
        description: "TBD",
        location: "TBD",
        media_count: "TBD",
      },
    ]}
  />
);
export default PreviewStage;
