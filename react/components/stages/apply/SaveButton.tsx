import React, { FunctionComponent } from "react";
import { Button, Form } from "antd";

import { useAppSelector } from "../../../hooks/state";
import { selectSaveEnabled } from "../../../state/slices/formStatus";

/**
 * Component for the button of the bulk edit form which can be used to save a
 * pending transaction.
 *
 * @return {ReactElement}
 *   A React element for the save button of the bulk edit form.
 */
const SaveButton: FunctionComponent = () => {
  const saveEnabled: boolean = useAppSelector(selectSaveEnabled);

  return (
    <Form.Item name="save">
      <Button
        className="bulk-editor-stage__button bulk-editor-stage__button--primary"
        disabled={!saveEnabled}
        htmlType="submit"
      >
        {Drupal.t("Save for later")}
      </Button>
    </Form.Item>
  );
};

export default SaveButton;
