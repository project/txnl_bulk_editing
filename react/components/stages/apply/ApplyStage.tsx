import React, { FunctionComponent } from "react";

import ApplyButton from "./ApplyButton";
import SaveButton from "./SaveButton";

/**
 * Component for the "Apply" stage of the bulk edit form.
 *
 * @return {ReactElement}
 *   A React element for the "Apply" stage of the bulk edit form.
 */
const ApplyStage: FunctionComponent = () => (
  <>
    <p>Apply this bulk edit now or save it for further editing later.</p>
    <div className="bulk-editor-button-group">
      <ApplyButton />
      <SaveButton />
    </div>
  </>
);

export default ApplyStage;
