import React, { FunctionComponent } from "react";
import { Button, Tooltip } from "antd";

/**
 * Component for a button which can be used to apply a transactional bulk edit.
 *
 * @return {ReactElement}
 *   React element for a button which can be used to apply a transactional bulk
 *   edit.
 */
const ApplyButton: FunctionComponent = () => (
  <Tooltip title={Drupal.t("Not yet supported")}>
    <Button
      type="primary"
      className="bulk-editor-stage__button bulk-editor-stage__button--primary"
      disabled
    >
      {Drupal.t("Apply now")}
    </Button>
  </Tooltip>
);

export default ApplyButton;
