import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

import type { AppDispatch, RootState } from "../state/store";

/**
 * Return a typed version of Redux's `dispatch` function for use in components.
 *
 * See the {@link
 * https://redux-toolkit.js.org/tutorials/typescript#define-typed-hooks Redux
 * Toolkit documentation about using useDispatch and useSelector in TypeScript}
 * for more information.
 *
 * @return {Function}
 *   A typed version of Redux's `dispatch` function for use in components.
 */
export const useAppDispatch = () => useDispatch<AppDispatch>();

/**
 * A typed version of Redux's `useSelector` hook for use in components.
 *
 * See the {@link
 * https://redux-toolkit.js.org/tutorials/typescript#define-typed-hooks Redux
 * Toolkit documentation about using useDispatch and useSelector in TypeScript}
 * for more information.
 */
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
