import React from "react";

export type Configuration = {
  editPagePath: string;
};

// Use an empty object as the default value. This is easier than setting a
// meaningful default in this file and it works fine, since the default is only
// used when a component does not have a matching provider above it in the tree,
// and we always use this context's provider.
//
// The use of `as` tells TypeScript to treat the empty object as if it has the
// needed type. The type checker would complain without this.
//
// https://reactjs.org/docs/context.html#reactcreatecontext
// https://github.com/typescript-cheatsheets/react/tree/1e09ba44e942a18dd54233c0c1d4a43e5fe77b6c#extended-example
const ConfigurationContext = React.createContext<Configuration>(
  {} as Configuration
);

export default ConfigurationContext;
