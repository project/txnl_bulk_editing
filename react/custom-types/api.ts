import type { NewResourceObject, ResourceObject } from "ts-json-api";
import type { Edit, Filter } from "@custom-types/fields";

// We know that the `query` object, if it exists, may contain a `value` prop.
// Declaring that expectation in this type allows us to avoid errors related to
// access of the `value` prop.
//
// https://github.com/rmarganti/json-api/issues/10#issuecomment-1122722727
export interface BulkEditResourceObject extends ResourceObject {
  attributes?: {
    query?: {
      value?: Filter;
    };
  };
}

export interface NewBulkEditResourceObject extends NewResourceObject {
  attributes?: {
    query?: {
      value?: Filter;
    };
  };
}
export interface EditResourceObject extends ResourceObject {
  attributes: {
    machine_name: string;
    mode: Edit.Method;
  };
}

export interface SingleValueEditResourceObject extends EditResourceObject {
  relationships: {
    search: {
      data: ResourceObject[];
    };
    replacement?: {
      data: ResourceObject;
    };
  };
}

export interface MultipleValuesEditResourceObject extends EditResourceObject {
  relationships: {
    search: {
      data: ResourceObject[];
    };
    replacement: {
      data: ResourceObject[];
    };
  };
}
