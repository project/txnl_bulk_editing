import type { Edit } from "@custom-types/fields";

export type FormValues = {
  filter: string | null;
  edits: Edit[] | null;
};
