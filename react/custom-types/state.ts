export enum FetchStatus {
  Idle,
  Pending,
  Fulfilled,
  Rejected,
}

export type Fetchable<D> = {
  status: FetchStatus;
  data: D | null;
};
