This directory is intentionally not named _types_.

We configure a path alias for this directory in _tsconfig.json_ which uses the
`@` character as a prefix. If this directory were named _types_, the path alias
would be `@types`, which would make it look like, and possibly even cause it to
conflict with, one of the many popular NPM packages that provide TypeScript
definitions, like `@types/react`.
