// These types would probably not be necessary if Drupal published TypeScript
// types. There is one contrib project which attempts to provide types, but it's
// not able to be installed via NPM and it hasn't been updated in a while.
//
// https://www.drupal.org/project/typings

export type DrupalContext = HTMLElement;

export type DrupalSettings = {
  path: {
    baseUrl: string;
  };
  txnl_bulk_editing: {
    editPagePath: string;
    transactionId: string;
  };
};

export type TermID = string;
