export type Filter = string;

export enum FieldType {
  Unknown,
  SingleValueTaxonomy,
  MultipleValueTaxonomy,
  Text,
}

export interface Edit {
  field: string;
  method: Edit.Method;
  "field-type"?: FieldType;
  "previously-used-uuid"?: string;
}

export namespace Edit {
  export type Method =
    | "replace_contents"
    | "replace_matches"
    | "add_values"
    | "remove_values";

  export namespace Taxonomy {
    export interface SingleValue extends Edit {
      method: "replace_contents" | "replace_matches";
    }

    export namespace SingleValue {
      export interface ReplaceContents extends SingleValue {
        method: "replace_contents";
        "taxonomy-single-replace-contents": string;
      }

      export interface ReplaceMatches extends SingleValue {
        method: "replace_matches";
        "taxonomy-single-replace-matches-old": string[];
        "taxonomy-single-replace-matches-new": string;
      }
    }

    export interface MultipleValues extends Edit {
      method:
        | "replace_contents"
        | "replace_matches"
        | "add_values"
        | "remove_values";
    }

    export namespace MultipleValues {
      export interface ReplaceContents extends MultipleValues {
        method: "replace_contents";
        "taxonomy-multiple-replace-contents": string[];
      }

      export interface ReplaceMatches extends MultipleValues {
        method: "replace_matches";
        "taxonomy-multiple-replace-matches-old": string[];
        "taxonomy-multiple-replace-matches-new": string[];
      }

      export interface AddValues extends MultipleValues {
        method: "add_values";
        "taxonomy-multiple-add": string[];
      }

      export interface RemoveValues extends MultipleValues {
        method: "remove_values";
        "taxonomy-multiple-remove": string[];
      }
    }
  }
}
