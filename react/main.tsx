import React from "react";
import ReactDOM from "react-dom";
import { Provider as ReduxProvider } from "react-redux";
import "antd/dist/antd.css";

import store, { initializeAPI } from "./state/store";
import ConfigurationContext, {
  Configuration,
} from "./contexts/configurationContext";
import BulkEditor from "./components/BulkEditor";
import { setTransactionID } from "./state/slices/persistence";
import "./main.css";

import type { DrupalContext, DrupalSettings } from "@custom-types/drupal";

Drupal.behaviors.txnl_bulk_editing = {
  attach: (context: DrupalContext, settings: DrupalSettings): void => {
    // Avoid unnecessary re-renders.
    // https://reactjs.org/docs/context.html#caveats
    const value: Configuration = {
      editPagePath: settings.txnl_bulk_editing.editPagePath,
    };

    initializeAPI(settings.path.baseUrl);

    // Store the transaction ID in Redux because, unlike `editPagePath`, it may
    // change in the future.
    store.dispatch(setTransactionID(settings.txnl_bulk_editing.transactionId));

    /**
     * Render the bulk edit form.
     */
    ReactDOM.render(
      <React.StrictMode>
        <ReduxProvider store={store}>
          <ConfigurationContext.Provider value={value}>
            <BulkEditor />
          </ConfigurationContext.Provider>
        </ReduxProvider>
      </React.StrictMode>,
      document.getElementById("root")
    );
  },
};
