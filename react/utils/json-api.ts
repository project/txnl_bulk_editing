import type { ResourceObject } from "ts-json-api";
import type { AppDispatch } from "../state/store";
import type { GetStateFunction } from "../api";

/**
 * Fetch all attributes from one or more pages of JSON:API results.
 *
 * @param {AppDispatch} dispatch
 *   The callback from Redux to dispatch the API call as a Redux action.
 * @param {GetStateFunction} getState
 *   The callback from Redux to get the state of the component.
 * @param {Function} apiMethod
 *   The method of the DrupalAPI class that should be used.
 * @param {Object} requestArgs
 *   An object containing the named arguments passed in the request URL.
 * @param {number} pageOffset
 *   The first page from which attributes should be returned.
 * @param {T[]} previousPagesAttributes
 *   An array of all attributes from previous pages of API results.
 *
 * @return {Promise<T[]>}
 *   A Promise that resolves to all attributes from one or more pages Drupal API
 *   results.
 */
export async function fetchPaginatedAttributes<T>(
  dispatch: AppDispatch,
  getState: GetStateFunction,
  apiMethod: Function,
  requestArgs: Object,
  pageOffset: number = 0,
  previousPagesAttributes: T[] = []
): Promise<T[]> {
  const response = await apiMethod(dispatch, getState, {
    ...requestArgs,
    "page[offset]": pageOffset,
  });

  const currentPageAttributes: T[] = response.body.data.map(
    ({ attributes }: ResourceObject) => attributes
  );

  const pagesThusfarAttributes: T[] = previousPagesAttributes.concat(
    currentPageAttributes
  );

  // If there is no next page, then this is the last page. Return all attributes
  // that we found.
  if (!response.body.links.next) {
    return pagesThusfarAttributes;
  }

  // Otherwise, tail recurse.
  return fetchPaginatedAttributes<T>(
    dispatch,
    getState,
    apiMethod,
    requestArgs,
    pageOffset + response.body.data.length,
    pagesThusfarAttributes
  );
}

/**
 * Given an array of JSON:API resource objects, return the IDs of those resource
 * objects.
 *
 * @param {ResourceObject[]} resourceObjects
 *   An array of resource objects.
 * @return {string[]}
 *   The IDs of the provided resource objects.
 */
export function getResourceObjectIDs(
  resourceObjects: ResourceObject[]
): string[] {
  return resourceObjects.map(({ id }: ResourceObject) => id);
}
