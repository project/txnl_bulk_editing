import type { Edit, Filter } from "@custom-types/fields";
import type { FormValues } from "@custom-types/form";
import type FormValuesBuilder from "./FormValuesBuilder";

/**
 * Class representing a Director, as described in the Gang of Four Builder
 * pattern, which can be used to direct the building of form values that can be
 * used by Ant.
 */
export default class FormValuesDirector {
  #formValuesBuilder: FormValuesBuilder;

  /**
   * Construct a new instance.
   *
   * @param {RequestBuilder<DataType>} formValuesBuilder
   *   The Builder, as described by the Gang of Four Builder pattern, that will
   *   be used to build the form values.
   */
  constructor(formValuesBuilder: FormValuesBuilder) {
    this.#formValuesBuilder = formValuesBuilder;
  }

  /**
   * Use the provided Builder to create a new request.
   *
   * @return {FormValues}
   *   The form values that were built by the Builder.
   */
  createFormValues(): FormValues {
    // The Gang of Four mention that returning values from Builder methods and
    // passing them around is usually not necessary, but doing so here helps us
    // to enforce type safety.

    const filter: Filter | null = this.#formValuesBuilder.buildFilter();
    const edits: Edit[] | null = this.#formValuesBuilder.buildEdits();
    return this.#formValuesBuilder.buildFormValues(filter, edits);
  }
}
