import {
  EditResourceObject,
  SingleValueEditResourceObject,
  MultipleValuesEditResourceObject,
} from "@custom-types/api";
import { FieldType } from "@custom-types/fields";
import BuilderError from "@utils/errors/BuilderError";

import FormValuesBuilder from "./FormValuesBuilder";
import SingleValueTaxonomyEditBuilder from "./edits/taxonomy/SingleValueTaxonomyEditBuilder";
import MultipleValuesTaxonomyEditBuilder from "./edits/taxonomy/MultipleValuesTaxonomyEditBuilder";

import type { ResourceObject } from "ts-json-api";
import type { Edit, Filter } from "@custom-types/fields";
import type { UUID } from "@custom-types/general";
import type { Field } from "../../../state/slices/fields";

/**
 * Class representing a concrete Builder, as described in the Gang of Four
 * Builder pattern, which can be used to build form values representing an
 * existing transaction. Ant can use these form values to populate form items.
 */
export default class RestoreTransactionFormValuesBuilder extends FormValuesBuilder {
  /**
   * @inheritdoc
   */
  buildFilter(): Filter | null {
    const filter: Filter | undefined =
      this.marshalledTransaction.data?.attributes?.query?.value;

    return filter !== undefined ? filter : null;
  }

  /**
   * Return the field type of a marshalled edit.
   *
   * @private
   * @param {EditResourceObject} marshalledEdit
   *   A marshalled edit.
   * @return {FieldType}
   *   The field type of the marshalled edit.
   */
  #getFieldType(marshalledEdit: EditResourceObject): FieldType {
    const fieldName: string = marshalledEdit.attributes.machine_name;
    const field: Field | undefined = this.fields[fieldName];

    if (field === undefined) {
      return FieldType.Unknown;
    } else {
      return field.type;
    }
  }

  /**
   * Return `true` if the provided edit is a properly-formatted marshalled edit,
   * or `false` otherwise. It's important to know this before using type
   * casting.
   *
   * @private
   * @param {ResourceObject} marshalledEdit
   *   A marshalled edit.
   * @return {boolean}
   *   `true` if the provided edit is a properly-formatted marshalled edit, or
   *   `false` otherwise.
   */
  #isValidMarshalledEdit(marshalledEdit: ResourceObject): boolean {
    // If the marshalled edit is not valid in general, return `false`.
    if (marshalledEdit.attributes === undefined) return false;
    if (marshalledEdit.relationships === undefined) return false;
    if (typeof marshalledEdit.attributes.machine_name !== "string")
      return false;

    const method: any = marshalledEdit.attributes.mode;
    const fieldType: FieldType = this.#getFieldType(
      marshalledEdit as EditResourceObject
    );

    // If the marshalled edit is not valid for its type, return `false`.
    switch (fieldType) {
      case FieldType.SingleValueTaxonomy:
        if (
          !["replace_contents", "replace_matches"].includes(method) ||
          !Array.isArray(marshalledEdit.relationships.search?.data) ||
          marshalledEdit.relationships.replacement === undefined
        ) {
          return false;
        }
        break;
      case FieldType.MultipleValueTaxonomy:
        if (
          ![
            "replace_contents",
            "replace_matches",
            "add_values",
            "remove_values",
          ].includes(method) ||
          !Array.isArray(marshalledEdit.relationships.search?.data) ||
          !Array.isArray(marshalledEdit.relationships.replacement?.data)
        ) {
          return false;
        }
        break;
      default:
        return false;
    }

    // Otherwise, return `true`.
    return true;
  }

  /**
   * Reducer function that can be passed to `Array.prototype.reduce()` to
   * unmarshall a single marshalled edit and return an array of accumulated
   * unmarshalled edits.
   *
   * See MDN's {@link
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce#parameters
   * `Array.prototype.reduce()`} documentation for more information.
   *
   * @private
   * @throws {BuilderError}
   *   If the marshalled edit is invalid.
   * @param {Edit[]} acc
   *   The value resulting from the previous call to this function.
   * @param {ResourceObject} marshalledEdit
   *   The current marshalled edit.
   * @return {Edit[]}
   *   An array of unmarshalled edits.
   */
  #editsReducer(acc: Edit[], marshalledEdit: ResourceObject): Edit[] {
    try {
      if (!this.#isValidMarshalledEdit(marshalledEdit)) {
        throw new BuilderError("Invalid edit returned from API.");
      }

      // It's safe to use `as` below because we have already verified that the
      // edit is an `EditResourceObject` by calling
      // `this.#isValidMarshalledEdit`.
      let unmarshalledEdit: Edit;
      const fieldType: FieldType = this.#getFieldType(
        marshalledEdit as EditResourceObject
      );

      switch (fieldType) {
        case FieldType.SingleValueTaxonomy: {
          // It's safe to use `as` below because we have already verified that
          // the edit is an `EditResourceObject` by calling
          // `this.#isValidMarshalledEdit` and its field type indicates that
          // it's a `SingleValueEditResourceObject` in particular.
          const editBuilder: SingleValueTaxonomyEditBuilder =
            new SingleValueTaxonomyEditBuilder(
              marshalledEdit as SingleValueEditResourceObject
            );
          unmarshalledEdit = editBuilder.buildEdit();
          break;
        }
        case FieldType.MultipleValueTaxonomy: {
          // It's safe to use `as` below because we have already verified that
          // the edit is an `EditResourceObject` by calling
          // `this.#isValidMarshalledEdit` and its field type indicates that
          // it's a `MultiValueEditResourceObject` in particular.
          const editBuilder: MultipleValuesTaxonomyEditBuilder =
            new MultipleValuesTaxonomyEditBuilder(
              marshalledEdit as MultipleValuesEditResourceObject
            );
          unmarshalledEdit = editBuilder.buildEdit();
          break;
        }
        default:
          throw new BuilderError(
            Drupal.t("Unknown marshalledEdit field type.")
          );
      }

      return [...acc, unmarshalledEdit];
    } catch (err) {
      // If an error is thrown while building this element, catch the error here
      // so that the element is not included in the array and attempts can
      // nonetheless be made to build other elements.
      return acc;
    }
  }

  /**
   * Return an array of all marshalled edits as returned by the API.
   *
   * @private
   * @return {ResourceObject[]}
   *   An array of all marshalled edits as returned by the API.
   */
  #getMarshalledEdits(): ResourceObject[] | null {
    // If there is no `included` array, there are no edits to return.
    if (this.marshalledTransaction.included === undefined) return null;

    const marshalledEditMetadata =
      this.marshalledTransaction?.data?.relationships?.edits?.data;

    // Return `null` if `marshalledEditMetadata` is `undefined` or not an array.
    // `Array.isArray()` returns `false` if the value it's passed is
    // `undefined`.
    if (!Array.isArray(marshalledEditMetadata)) return null;

    // The UUIDs of edits whose data _should_ be included in this resource
    // object, from the metadata provided in the resource object.
    const editUUIDs: UUID[] = marshalledEditMetadata.map(
      ({ id }: ResourceObject) => id
    );

    // Data about edits that match the UUIDs in the metadata.
    const unmarshalledEdits: ResourceObject[] =
      this.marshalledTransaction.included.filter(({ id }: ResourceObject) =>
        editUUIDs.includes(id)
      );

    if (unmarshalledEdits.length) {
      return unmarshalledEdits;
    } else {
      return null;
    }
  }

  /**
   * @inheritdoc
   */
  buildEdits(): Edit[] | null {
    const marshalledEdits: ResourceObject[] | null = this.#getMarshalledEdits();

    if (marshalledEdits === null) return null;

    // Bind `this.#editsReducer` to this method's value of `this`. If we didn't
    // do this, it would be `undefined` in `this.#editsReducer`.
    //
    // https://stackoverflow.com/a/59060545
    const unmarshalledEdits: Edit[] = marshalledEdits.reduce(
      this.#editsReducer.bind(this),
      []
    );

    if (unmarshalledEdits.length) {
      return unmarshalledEdits;
    } else {
      return null;
    }
  }
}
