import type { Response } from "ts-json-api";
import type { BulkEditResourceObject } from "@custom-types/api";
import type { Edit, Filter } from "@custom-types/fields";
import type { FormValues } from "@custom-types/form";
import type { Fields } from "../../../state/slices/fields";

/**
 * Class representing an abstract Builder, as described in the Gang of Four
 * Builder pattern, which specifies an interface for building form values that
 * can be used by Ant and which implements some of that building.
 */
export default abstract class FormValuesBuilder {
  protected marshalledTransaction: Response<BulkEditResourceObject>;

  protected fields: Fields;

  /**
   * Construct a new instance.
   *
   * @param {Response<BulkEditResourceObject>} marshalledTransaction
   *   A marshalled (i.e., raw and not yet processed) bulk edit transaction as
   *   returned by JSON:API.
   * @param {Fields} fields
   *   Formatted information about all fields displayed in the Bulk Editor.
   */
  constructor(
    marshalledTransaction: Response<BulkEditResourceObject>,
    fields: Fields
  ) {
    this.marshalledTransaction = marshalledTransaction;
    this.fields = fields;
  }

  /**
   * Return the unmarshalled filter of the transaction that is being restored,
   * or `null` if the transaction being restored does not include a filter.
   *
   * @return {Filter | null}
   *   The unmarshalled filter of the transaction that is being restored, or
   *   `null` if the transaction being restored does not include a filter.
   */
  buildFilter(): Filter | null {
    return null;
  }

  /**
   * Return an array of unmarshalled edits from the restored transaction, or
   * `null` if the transaction being restored does not include edits.
   *
   * @return {Filter | null}
   *   An array of unmarshalled edits from the transaction being restored, or
   *   `null` if the transaction being restored does not include edits.
   */
  buildEdits(): Edit[] | null {
    return null;
  }

  /**
   * Return form values that can be used by Ant to populate form fields.
   *
   * @param {Filter |  null} filter
   *   The unmarshalled filter of the transaction that is being restored, or
   *   `null` if the transaction being restored does not include a filter.
   * @param {Edit[] | null} edits
   *   An array of unmarshalled edits from the restored transaction, or `null`
   *   if the transaction being restored does not include edits.
   * @return {FormValues}
   *   Form values that can be used by Ant to populate form fields.
   */
  buildFormValues(filter: Filter | null, edits: Edit[] | null): FormValues {
    return {
      filter,
      edits,
    };
  }
}
