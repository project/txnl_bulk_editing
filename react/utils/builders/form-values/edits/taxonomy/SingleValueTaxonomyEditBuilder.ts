import TaxonomyEditBuilder from "@utils/builders/form-values/edits/taxonomy/TaxonomyEditBuilder";
import CannotBuildEditError from "@utils/errors/CannotBuildEditError";
import { getResourceObjectIDs } from "@utils/json-api";

import type { ResourceObject } from "ts-json-api";
import type { SingleValueEditResourceObject } from "@custom-types/api";
import type { Edit } from "@custom-types/fields";

/**
 * Class representing an error that can be thrown if it is not possible to
 * unmarshall a single-value taxonomy edit.
 */
class CannotBuildSingleValueTaxonomyEditError extends CannotBuildEditError {
  /**
   * Construct a new instance.
   *
   * @param {string} method
   *   The human-readable method of the edit.
   */
  constructor(method: string) {
    super("single-value taxonomy", method);
    this.name = "CannotBuildSingleValueEditError";
  }
}

/**
 * Class which can be used to build an unmarshalled edit to a single-value
 * taxonomy field.
 */
export default class SingleValueTaxonomyEditBuilder extends TaxonomyEditBuilder {
  #marshalledEdit: SingleValueEditResourceObject;

  /**
   * Construct a new instance.
   *
   * @param {MultipleValuesEditResourceObject} marshalledEdit
   *   A marshalled edit to a multiple-value taxonomy field.
   */
  constructor(marshalledEdit: SingleValueEditResourceObject) {
    super();
    this.#marshalledEdit = marshalledEdit;
  }

  /**
   * Return an unmarshalled edit which replaces the contents of a single-value
   * taxonomy field.
   *
   * @private
   * @throws {CannotBuildSingleValueTaxonomyEditError}
   *   If the edit cannot be unmarshalled.
   * @return {Edit.Taxonomy.SingleValue.ReplaceContents}
   *   An unmarshalled edit which replaces the contents of a single-value
   *   taxonomy field.
   */
  #buildReplaceContents(): Edit.Taxonomy.SingleValue.ReplaceContents {
    const replacementTermID: string | undefined | null =
      this.#marshalledEdit?.relationships?.replacement?.data.id;

    if (replacementTermID === undefined || replacementTermID === null) {
      throw new CannotBuildSingleValueTaxonomyEditError("replace_contents");
    }

    return {
      field: this.#marshalledEdit.attributes.machine_name,
      method: "replace_contents",
      "taxonomy-single-replace-contents": replacementTermID,
    };
  }

  /**
   * Return an unmarshalled edit which replaces matches in a single-value
   * taxonomy field.
   *
   * @private
   * @throws {CannotBuildSingleValueTaxonomyEditError}
   *   If the edit cannot be unmarshalled.
   * @return {Edit.Taxonomy.SingleValue.ReplaceMatches}
   *   An unmarshalled edit which replaces matches in a single-value taxonomy
   *   field.
   */
  #buildReplaceMatches(): Edit.Taxonomy.SingleValue.ReplaceMatches {
    const replacementTermID: string | undefined | null =
      this.#marshalledEdit?.relationships?.replacement?.data.id;

    const searchTermData: ResourceObject[] | undefined =
      this.#marshalledEdit?.relationships?.search?.data;

    if (
      replacementTermID === undefined ||
      replacementTermID === null ||
      searchTermData === undefined
    ) {
      throw new CannotBuildSingleValueTaxonomyEditError("replace_matches");
    }

    return {
      field: this.#marshalledEdit.attributes.machine_name,
      method: "replace_matches",
      "taxonomy-single-replace-matches-old":
        getResourceObjectIDs(searchTermData),
      "taxonomy-single-replace-matches-new": replacementTermID,
    };
  }

  /**
   * @inheritDoc
   */
  buildEdit(): Edit {
    switch (this.#marshalledEdit.attributes.mode) {
      case "replace_contents":
        return this.#buildReplaceContents();
      case "replace_matches":
        return this.#buildReplaceMatches();
      default:
        throw new CannotBuildSingleValueTaxonomyEditError(
          Drupal.t("Unknown single-value taxonomy edit method.")
        );
    }
  }
}
