import TaxonomyEditBuilder from "@utils/builders/form-values/edits/taxonomy/TaxonomyEditBuilder";
import CannotBuildEditError from "@utils/errors/CannotBuildEditError";
import { getResourceObjectIDs } from "@utils/json-api";

import type { ResourceObject } from "ts-json-api";
import type { MultipleValuesEditResourceObject } from "@custom-types/api";
import type { Edit } from "@custom-types/fields";

/**
 * Class representing an error that can be thrown if it is not possible to
 * unmarshall a multiple-value taxonomy edit.
 */
class CannotBuildMultipleValueTaxonomyEditError extends CannotBuildEditError {
  /**
   * Construct a new instance.
   *
   * @param {string} method
   *   The human-readable method of the edit.
   */
  constructor(method: string) {
    super("multiple-value taxonomy", method);
    this.name = "CannotBuildMultipleValueEditError";
  }
}

/**
 * Class which can be used to build an unmarshalled edit to a multiple-value
 * taxonomy field.
 */
export default class MultipleValuesTaxonomyEditBuilder extends TaxonomyEditBuilder {
  #marshalledEdit: MultipleValuesEditResourceObject;

  /**
   * Construct a new instance.
   *
   * @param {MultipleValuesEditResourceObject} marshalledEdit
   *   A marshalled edit to a multiple-value taxonomy field.
   */
  constructor(marshalledEdit: MultipleValuesEditResourceObject) {
    super();
    this.#marshalledEdit = marshalledEdit;
  }

  /**
   * Return an unmarshalled edit which replaces the contents of a multiple-value
   * taxonomy field.
   *
   * @private
   * @throws {CannotBuildMultipleValueTaxonomyEditError}
   *   If the edit method is unknown.
   * @return {Edit.Taxonomy.MultipleValues.ReplaceContents}
   *   An unmarshalled edit which replaces the contents of a multiple-value
   *   taxonomy field.
   */
  #buildReplaceContents(): Edit.Taxonomy.MultipleValues.ReplaceContents {
    const replacementTermsData: ResourceObject[] | undefined =
      this.#marshalledEdit?.relationships?.replacement?.data;

    if (replacementTermsData === undefined) {
      throw new CannotBuildMultipleValueTaxonomyEditError("replace_contents");
    }

    return {
      field: this.#marshalledEdit.attributes.machine_name,
      method: "replace_contents",
      "taxonomy-multiple-replace-contents":
        getResourceObjectIDs(replacementTermsData),
    };
  }

  /**
   * Return an unmarshalled edit which replaces matches in a multiple-value
   * taxonomy field.
   *
   * @private
   * @throws {CannotBuildMultipleValueTaxonomyEditError}
   *   If the edit cannot be unmarshalled.
   * @return {Edit.Taxonomy.MultipleValues.ReplaceMatches}
   *   An unmarshalled edit which replaces matches in a multiple-value taxonomy
   *   field.
   */
  #buildReplaceMatches(): Edit.Taxonomy.MultipleValues.ReplaceMatches {
    const oldTermsData: ResourceObject[] | undefined =
      this.#marshalledEdit?.relationships?.search?.data;

    const newTermsData: ResourceObject[] | undefined =
      this.#marshalledEdit?.relationships?.replacement?.data;

    if (oldTermsData === undefined || newTermsData === undefined) {
      throw new CannotBuildMultipleValueTaxonomyEditError("replace_matches");
    }

    return {
      field: this.#marshalledEdit.attributes.machine_name,
      method: "replace_matches",
      "taxonomy-multiple-replace-matches-old":
        getResourceObjectIDs(oldTermsData),
      "taxonomy-multiple-replace-matches-new":
        getResourceObjectIDs(newTermsData),
    };
  }

  /**
   * Return an unmarshalled edit which adds values to a multiple-value taxonomy
   * field.
   *
   * @private
   * @throws {CannotBuildMultipleValueTaxonomyEditError}
   *   If the edit cannot be unmarshalled.
   * @return {Edit.Taxonomy.MultipleValues.AddValues}
   *   An unmarshalled edit which adds values to a multiple-value taxonomy
   *   field.
   */
  #buildAddValues(): Edit.Taxonomy.MultipleValues.AddValues {
    const newTermsData: ResourceObject[] | undefined =
      this.#marshalledEdit?.relationships?.replacement?.data;

    if (newTermsData === undefined) {
      throw new CannotBuildMultipleValueTaxonomyEditError("add_values");
    }

    return {
      field: this.#marshalledEdit.attributes.machine_name,
      method: "add_values",
      "taxonomy-multiple-add": getResourceObjectIDs(newTermsData),
    };
  }

  /**
   * Return an unmarshalled edit which removes values from a multiple-value
   * taxonomy field.
   *
   * @private
   * @throws {CannotBuildMultipleValueTaxonomyEditError}
   *   If the edit cannot be unmarshalled.
   * @return {Edit.Taxonomy.MultipleValues.RemoveValues}
   *   An unmarshalled edit which removes values from a multiple-value taxonomy
   *   field.
   */
  #buildRemoveValues(): Edit.Taxonomy.MultipleValues.RemoveValues {
    const oldTermsData: ResourceObject[] | undefined =
      this.#marshalledEdit?.relationships?.search?.data;

    if (oldTermsData === undefined) {
      throw new CannotBuildMultipleValueTaxonomyEditError("remove_values");
    }

    return {
      field: this.#marshalledEdit.attributes.machine_name,
      method: "remove_values",
      "taxonomy-multiple-remove": getResourceObjectIDs(oldTermsData),
    };
  }

  /**
   * @inheritdoc
   */
  buildEdit(): Edit {
    switch (this.#marshalledEdit.attributes.mode) {
      case "replace_contents":
        return this.#buildReplaceContents();
      case "replace_matches":
        return this.#buildReplaceMatches();
      case "add_values":
        return this.#buildAddValues();
      case "remove_values":
        return this.#buildRemoveValues();
      default:
        throw new CannotBuildMultipleValueTaxonomyEditError(
          Drupal.t("Unknown multiple-value taxonomy edit method.")
        );
    }
  }
}
