import type { Edit } from "@custom-types/fields";

/**
 * Class which specifies an interface for building unmarshalled edits to
 * taxonomy fields.
 */
export default abstract class TaxonomyEditBuilder {
  /**
   * Return an unmarshalled edit to a taxonomy field.
   *
   * @private
   * @throws {CannotBuildEditError}
   *   If the edit method is unknown.
   * @return {Edit}
   *   An unmarshalled edit to a taxonomy field.
   */
  abstract buildEdit(): Edit;
}
