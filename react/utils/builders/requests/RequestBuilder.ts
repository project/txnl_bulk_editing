import type {
  Attributes,
  Relationships,
  Request,
  ResourceObject,
} from "ts-json-api";
import type {
  BulkEditResourceObject,
  NewBulkEditResourceObject,
} from "@custom-types/api";
import type { FormValues } from "@custom-types/form";
import type { Fields } from "../../../state/slices/fields";

/**
 * Class representing an abstract Builder, as described in the Gang of Four
 * Builder pattern, which specifies an interface for building requests and which
 * implements some of that building.
 */
export default abstract class RequestBuilder<
  DataType extends BulkEditResourceObject | NewBulkEditResourceObject
> {
  protected transactionType: string;

  protected formValues: FormValues;

  protected fields: Fields;

  /**
   * Construct a new instance.
   *
   * @protected
   * @param {string} transactionType
   *   The `type` that should be included in the top-level `data` object.
   * @param {FormValues} formValues
   *   An object which contains the values of all form items in the Bulk Editor.
   * @param {Fields} fields
   *   Formatted information about all fields displayed in the Bulk Editor.
   */
  protected constructor(
    transactionType: string,
    formValues: FormValues,
    fields: Fields
  ) {
    this.transactionType = transactionType;
    this.formValues = formValues;
    this.fields = fields;
  }

  /**
   * Return the `attributes` object of the top-level `data` object, or `null` if
   * the request does not need to include this object.
   *
   * @return {Attributes | null}
   *   The `attributes` object of the top-level `data` object, or null if the
   *   request does not need to include this object.
   */
  buildRequestDataAttributes(): Attributes | null {
    // Return `null` by default because some subclasses may not implement this
    // method.
    return null;
  }

  /**
   * Return the `relationships` object of the top-level `data` object, or `null`
   * if the request does not need to include this object.
   *
   * @return {Relationships | null}
   *   The `relationships` object of the top-level `data` object, or `null` if
   *   the request does not need to include this object.
   */
  buildRequestDataRelationships(): Relationships | null {
    // Return `null` by default because some subclasses may not implement this
    // method.
    return null;
  }

  /**
   * Return the top-level `data` object of the request.
   *
   * @abstract
   * @param {Attributes | null}
   *   The `attributes` object that should be included in the top-level `data`
   *   object, or null if no `attributes` object should be included.
   * @param {Relationships | null}
   *   The `relationships` object that should be included in the top-level
   *   `data` object, or null if no `relationships` object should be included.
   * @return {DataType}
   *   The top-level `data` object of the request.
   */
  abstract buildRequestData(
    requestAttributes: Attributes | null,
    requestRelationships: Relationships | null
  ): DataType;

  /**
   * Return the top-level `included` array of objects of the request, or `null`
   * if the request does not need to include this array.
   *
   * @return {ResourceObject[] | null}
   *   The top-level `included` array of objects of the request, or `null` if
   *   the request does not need to include this object.
   *
   */
  buildRequestIncluded(): ResourceObject[] | null {
    // Return `null` by default because some subclasses may not implement this
    // method.
    return null;
  }

  /**
   * Return the fully-built request.
   *
   * @param {DataType} requestData
   *   The `data` object that should be included in the request.
   * @param {ResourceObject[] | null} requestIncluded
   *   The `included` array of objects that should be included in the request,
   *   or null if the request does not need to include this array.
   * @return {Request<DataType>}
   *   The fully-built request.
   */
  buildRequest(
    requestData: DataType,
    requestIncluded: ResourceObject[] | null
  ): Request<DataType> {
    const request: Request<DataType> = {
      data: requestData,
    };

    if (requestIncluded !== null) {
      request.included = requestIncluded;
    }

    return request;
  }
}
