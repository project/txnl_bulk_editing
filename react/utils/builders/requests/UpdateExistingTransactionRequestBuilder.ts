import RequestBuilder from "./RequestBuilder";
import SaveNewTransactionRequestBuilder from "./SaveNewTransactionRequestBuilder";

import type { Attributes, Relationships, ResourceObject } from "ts-json-api";
import type { BulkEditResourceObject } from "@custom-types/api";
import type { FormValues } from "@custom-types/form";
import type { Fields } from "../../../state/slices/fields";
import type { TransactionID } from "../../../state/slices/persistence";

/**
 * Class representing a concrete Builder, as described in the Gang of Four
 * Builder pattern, which can be used to build a request to update an existing
 * bulk edit transaction.
 */
export default class UpdateExistingTransactionRequestBuilder extends RequestBuilder<BulkEditResourceObject> {
  #saveNewTransactionRequestBuilder: SaveNewTransactionRequestBuilder;

  #transactionID: TransactionID;

  /**
   * Construct a new instance.
   *
   * @param {string} transactionType
   *   The `type` that should be included in the top-level `data` resource
   *   object.
   * @param {FormValues} formValues
   *   An object which contains the values of all form items in the Bulk Editor.
   * @param {Fields} fields
   *   Formatted information about all fields displayed in the Bulk Editor.
   * @param {TransactionID} transactionID
   *   The ID of the transaction that should be overwritten.
   */
  constructor(
    transactionType: string,
    formValues: FormValues,
    fields: Fields,
    transactionID: TransactionID
  ) {
    super(transactionType, formValues, fields);

    this.#transactionID = transactionID;

    // Use composition to simplify this implementation.
    this.#saveNewTransactionRequestBuilder =
      new SaveNewTransactionRequestBuilder(transactionType, formValues, fields);
  }

  /**
   * @inheritdoc
   */
  buildRequestDataAttributes(): Attributes | null {
    return this.#saveNewTransactionRequestBuilder.buildRequestDataAttributes();
  }

  /**
   * @inheritdoc
   */
  buildRequestDataRelationships(): Relationships | null {
    return this.#saveNewTransactionRequestBuilder.buildRequestDataRelationships();
  }

  /**
   * @inheritdoc
   */
  buildRequestData(
    requestDataAttributes: Attributes | null,
    requestDataRelationships: Relationships | null
  ): BulkEditResourceObject {
    // This is the most important difference between this class and
    // `SaveNewTransactionBuilder`. We add an ID to the `data` resource object
    // to overwrite the existing transaction with the same ID.
    return {
      id: this.#transactionID,
      ...this.#saveNewTransactionRequestBuilder.buildRequestData(
        requestDataAttributes,
        requestDataRelationships
      ),
    };
  }

  /**
   * @inheritdoc
   */
  buildRequestIncluded(): ResourceObject[] | null {
    return this.#saveNewTransactionRequestBuilder.buildRequestIncluded();
  }
}
