import TaxonomyRelationshipsBuilder from "@utils/builders/requests/relationships/taxonomy/TaxonomyRelationshipsBuilder";
import BuilderError from "@utils/errors/BuilderError";

import type { Relationships } from "ts-json-api";
import type { TermID } from "@custom-types/drupal";
import type { Edit } from "@custom-types/fields";

/**
 * Class which can be used to build marshalled relationships for an edit to a
 * multiple-value taxonomy field.
 */
export default class MultipleValuesTaxonomyRelationshipsBuilder extends TaxonomyRelationshipsBuilder {
  #unmarshalledEdit: Edit.Taxonomy.MultipleValues;

  /**
   * Construct a new instance.
   *
   * @param {Edit.Taxonomy.MultipleValues} unmarshalledEdit
   *   An unmarshalled edit.
   * @param {string} resourceType
   *   The resource type of the field that is being edited.
   */
  constructor(
    unmarshalledEdit: Edit.Taxonomy.MultipleValues,
    resourceType: string
  ) {
    super(resourceType);
    this.#unmarshalledEdit = unmarshalledEdit;
  }

  /**
   * Return the relationship that provides data for the given edit to a
   * multiple-value taxonomy field, given that the edit replaces the contents of
   * the field.
   *
   * @private
   * @param {Edit.Taxonomy.MultipleValues.ReplaceContents} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   The relationship that provides data for the given edit.
   */
  #buildReplaceContents(
    unmarshalledEdit: Edit.Taxonomy.MultipleValues.ReplaceContents
  ): Relationships {
    const termIDs: TermID[] =
      unmarshalledEdit["taxonomy-multiple-replace-contents"];

    return {
      replacement: {
        data: termIDs.map((id: string) => {
          return {
            type: this.resourceType,
            id,
          };
        }),
      },
    };
  }

  /**
   * Return the relationships that provide data for the given edit to a
   * multiple-value taxonomy field, given that the edit replaces matches in the
   * field.
   *
   * @private
   * @param {Edit.Taxonomy.MultipleValues.ReplaceMatches} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   The relationship that provides data for the given edit.
   */
  #buildReplaceMatches(
    unmarshalledEdit: Edit.Taxonomy.MultipleValues.ReplaceMatches
  ): Relationships {
    const oldTermIDs: TermID[] =
      unmarshalledEdit["taxonomy-multiple-replace-matches-old"];
    const newTermIDs: TermID[] =
      unmarshalledEdit["taxonomy-multiple-replace-matches-new"];

    return {
      search: {
        data: oldTermIDs.map((id: string) => {
          return {
            type: this.resourceType,
            id,
          };
        }),
      },
      replacement: {
        data: newTermIDs.map((id: string) => {
          return {
            type: this.resourceType,
            id,
          };
        }),
      },
    };
  }

  /**
   * Return the relationship that provides data for the given edit to a
   * multiple-value taxonomy field, given that the edit adds values to the
   * field.
   *
   * @private
   * @param {Edit.Taxonomy.MultipleValues.AddValues} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   The relationship that provides data for the given edit.
   */
  #buildAddValues(
    unmarshalledEdit: Edit.Taxonomy.MultipleValues.AddValues
  ): Relationships {
    const termIDs: TermID[] = unmarshalledEdit["taxonomy-multiple-add"];

    return {
      replacement: {
        data: termIDs.map((id: string) => {
          return {
            type: this.resourceType,
            id,
          };
        }),
      },
    };
  }

  /**
   * Return the relationship that provides data for the given edit to a
   * multiple-value taxonomy field, given that the edit removes values from the
   * field.
   *
   * @private
   * @param {Edit.Taxonomy.MultipleValues.RemoveValues} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   The relationship that provides data for the given edit.
   */
  #buildRemoveValues(
    unmarshalledEdit: Edit.Taxonomy.MultipleValues.RemoveValues
  ): Relationships {
    const termIDs: TermID[] = unmarshalledEdit["taxonomy-multiple-remove"];

    return {
      search: {
        data: termIDs.map((id: string) => {
          return {
            type: this.resourceType,
            id,
          };
        }),
      },
    };
  }

  /**
   * @inheritdoc
   */
  buildRelationships(): Relationships {
    switch (this.#unmarshalledEdit.method) {
      case "replace_contents":
        return this.#buildReplaceContents(
          this.#unmarshalledEdit as Edit.Taxonomy.MultipleValues.ReplaceContents
        );
      case "replace_matches":
        return this.#buildReplaceMatches(
          this.#unmarshalledEdit as Edit.Taxonomy.MultipleValues.ReplaceMatches
        );
      case "add_values":
        return this.#buildAddValues(
          this.#unmarshalledEdit as Edit.Taxonomy.MultipleValues.AddValues
        );
      case "remove_values":
        return this.#buildRemoveValues(
          this.#unmarshalledEdit as Edit.Taxonomy.MultipleValues.RemoveValues
        );
      default:
        throw new BuilderError(
          Drupal.t("Unknown multiple-value taxonomy edit method.")
        );
    }
  }
}
