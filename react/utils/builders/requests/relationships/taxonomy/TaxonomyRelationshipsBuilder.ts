import type { Relationships } from "ts-json-api";
import type { Edit } from "@custom-types/fields";

/**
 * Class which specifies an interface for building marshalled relationships for
 * edits to taxonomy fields.
 */
export default abstract class TaxonomyRelationshipsBuilder {
  protected resourceType: string;

  /**
   * Construct a new instance.
   *
   * @param {string} resourceType
   *   The resource type of the field that is being edited.
   */
  constructor(resourceType: string) {
    this.resourceType = resourceType;
  }

  /**
   * Return one or more relationships that provide data for the given edit to a
   * taxonomy field.
   *
   * @private
   * @throws {BuilderError}
   *   If the edit method is unknown.
   * @param {Edit.Taxonomy.MultipleValues} unmarshalledEdit
   *   The unmarshalled edit.
   * @param {string} resourceType
   *   The resource type of the field that is being edited.
   * @return {Relationships}
   *   One or more relationships that should be added to this request to provide
   *   data for the provided edit to a multiple-value taxonomy field.
   */
  abstract buildRelationships(
    unmarshalledEdit: Edit.Taxonomy.MultipleValues
  ): Relationships;
}
