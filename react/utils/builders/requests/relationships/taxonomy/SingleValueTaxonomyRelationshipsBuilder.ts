import TaxonomyRelationshipsBuilder from "@utils/builders/requests/relationships/taxonomy/TaxonomyRelationshipsBuilder";
import BuilderError from "@utils/errors/BuilderError";

import type { Relationships } from "ts-json-api";
import type { TermID } from "@custom-types/drupal";
import type { Edit } from "@custom-types/fields";

/**
 * Class which can be used to build marshalled relationships for an edit to a
 * single-value taxonomy field.
 */
export default class SingleValueTaxonomyRelationshipsBuilder extends TaxonomyRelationshipsBuilder {
  #unmarshalledEdit: Edit.Taxonomy.SingleValue;

  /**
   * Construct a new instance.
   *
   * @param {Edit.Taxonomy.SingleValue} unmarshalledEdit
   *   An unmarshalled edit.
   * @param {string} resourceType
   *   The resource type of the field that is being edited.
   */
  constructor(
    unmarshalledEdit: Edit.Taxonomy.SingleValue,
    resourceType: string
  ) {
    super(resourceType);
    this.#unmarshalledEdit = unmarshalledEdit;
  }

  /**
   * Return the relationship that provides data for the given edit to a
   * single-value taxonomy field, given that the edit replaces the contents of
   * the field.
   *
   * @private
   * @param {Edit.Taxonomy.SingleValue.ReplaceContents} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   The relationship that provides data for the given edit.
   */
  #buildReplaceContents(
    unmarshalledEdit: Edit.Taxonomy.SingleValue.ReplaceContents
  ): Relationships {
    return {
      replacement: {
        data: {
          type: this.resourceType,
          id: unmarshalledEdit["taxonomy-single-replace-contents"],
        },
      },
    };
  }

  /**
   * Return the relationship that provides data for the given edit to a
   * single-value taxonomy field, given that the edit replaces matches in the
   * field.
   *
   * @private
   * @param {Edit.Taxonomy.SingleValue.ReplaceContents} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   The relationship that provides data for the given edit.
   */
  #buildReplaceMatches(
    unmarshalledEdit: Edit.Taxonomy.SingleValue.ReplaceMatches
  ): Relationships {
    const oldTermIDs: TermID[] =
      unmarshalledEdit["taxonomy-single-replace-matches-old"];

    return {
      search: {
        data: oldTermIDs.map((id: string) => {
          return {
            type: this.resourceType,
            id,
          };
        }),
      },
      replacement: {
        data: {
          type: this.resourceType,
          id: unmarshalledEdit["taxonomy-single-replace-matches-new"],
        },
      },
    };
  }

  /**
   * Return one or more relationships that should be added to this request to
   * provide data for the provided edit to a single-value taxonomy field.
   *
   * @private
   * @param {Edit.Taxonomy.SingleValue} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {Relationships}
   *   One or more relationships that should be added to this request to provide
   *   data for the provided edit to a single-value taxonomy field.
   */
  buildRelationships(): Relationships {
    switch (this.#unmarshalledEdit.method) {
      case "replace_contents":
        return this.#buildReplaceContents(
          this.#unmarshalledEdit as Edit.Taxonomy.SingleValue.ReplaceContents
        );
      case "replace_matches":
        return this.#buildReplaceMatches(
          this.#unmarshalledEdit as Edit.Taxonomy.SingleValue.ReplaceMatches
        );
      default:
        throw new BuilderError(
          Drupal.t("Unknown single-value taxonomy edit method.")
        );
    }
  }
}
