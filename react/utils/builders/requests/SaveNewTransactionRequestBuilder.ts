import { v4 as uuidv4 } from "uuid";
import BuilderError from "@utils/errors/BuilderError";
import { FieldType } from "@custom-types/fields";

import RequestBuilder from "./RequestBuilder";
import MultipleValuesTaxonomyRelationshipsBuilder from "./relationships/taxonomy/MultipleValuesTaxonomyRelationshipsBuilder";
import SingleValueTaxonomyRelationshipsBuilder from "./relationships/taxonomy/SingleValueTaxonomyRelationshipsBuilder";

import type { Attributes, Relationships, ResourceObject } from "ts-json-api";
import type { NewBulkEditResourceObject } from "@custom-types/api";
import type { Edit } from "@custom-types/fields";
import type { FormValues } from "@custom-types/form";
import type { UUID } from "@custom-types/general";
import type { Field, Fields } from "../../../state/slices/fields";

/**
 * Class representing a concrete Builder, as described in the Gang of Four
 * Builder pattern, which can be used to build a request to save a new bulk edit
 * transaction.
 */
export default class SaveNewTransactionRequestBuilder extends RequestBuilder<NewBulkEditResourceObject> {
  #savableEdits: Edit[];

  #editUUIDs: UUID[];

  /**
   * @inheritdoc
   */
  constructor(transactionType: string, formValues: FormValues, fields: Fields) {
    super(transactionType, formValues, fields);

    this.#savableEdits = this.#getSavableEdits();
    this.#editUUIDs = this.#getEditUUIDs(this.#savableEdits.length);
  }

  /**
   * Return an array of unmarshalled edits that can be saved.
   *
   * TODO (ARKNG-1002): Do not filter out "unsavable" edits.
   *
   * As we build the MVP, there may be times when UI is available for editing
   * certain types of fields, but saving those edits is not yet supported. That
   * is why this filtering is necessary.
   *
   * When the MVP is completed, however, it seems very unlikely that there will
   * be any field types which can be edited without those edits being able to be
   * saved. If that is the case, then we can forgo this filtering at that time.
   *
   * At that same time, we may be able to remove the `FieldType` component
   * because, at the time of this writing, the `FieldType` serves only to
   * facilitate this filtering. On the other hand, perhaps it would be good to
   * keep it around, in case, at some point in the future, we do need field type
   * information while saving.
   *
   * @return {Edit[]}
   *   An array of unmarshalled edits that can be saved.
   */
  #getSavableEdits(): Edit[] {
    if (this.formValues.edits === null) return [];

    const savableFieldTypes: FieldType[] = [
      FieldType.SingleValueTaxonomy,
      FieldType.MultipleValueTaxonomy,
    ];

    return this.formValues.edits.filter((edit: Edit): boolean => {
      const fieldType: FieldType = this.#getFieldType(edit);
      return fieldType !== undefined && savableFieldTypes.includes(fieldType);
    });
  }

  /**
   * Return a UUID for the edit with the given index position in the collection
   * of form values. If the edit already has a UUID assigned to it, return that
   * UUID. Otherwise, return a new UUID.
   *
   * @param {numbers} index
   *   The index position of the edit in the collection of form values.
   * @return {UUID[]}
   *   A UUID for the provided edit.
   */
  #getEditUUID(index: number): UUID {
    const previouslyUsedUUID: string | undefined =
      this.#savableEdits?.[index]?.["previously-used-uuid"];

    // If this transaction was previously saved, re-use the UUID that Drupal
    // provided for this edit when it was restored, so that we overwrite that
    // edit instead of creating a new one.
    if (previouslyUsedUUID !== undefined) {
      return previouslyUsedUUID;
    }

    // Otherwise, create a new, random UUID here on the client side. It can have
    // any value as long as it agrees with the corresponding `id` in the
    // included resources of the overall request.
    return uuidv4();
  }

  /**
   * Return an array of UUIDs, one for each edit that exists in this bulk edit
   * transaction. If an edit already had a UUID assigned to it, re-use that
   * UUID.
   *
   * @param {numbers} numEdits
   *   The number of edits in this transaction.
   * @return {UUID[]}
   *   An array of UUIDs, one for each edit that exists in this bulk edit
   *   transaction.
   */
  #getEditUUIDs(numEdits: number): UUID[] {
    // This looks pretty strange, but it's not that complicated. It returns a
    // new array of length `numEdits` where each element of the array is created
    // by calling `this.#getEditUUID`.
    //
    // https://stackoverflow.com/a/54342226
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/from#Using_arrow_functions_and_Array.from
    return Array.from(
      { length: numEdits },
      (element: undefined, index: number) => this.#getEditUUID(index)
    );
  }

  /**
   * Return the field type of the provided edit.
   *
   * @private
   * @param {Edit} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {FieldType}
   *   The field type of the provided edit.
   */
  #getFieldType(unmarshalledEdit: Edit): FieldType {
    const field: Field | undefined = this.fields[unmarshalledEdit.field];
    return field === undefined ? FieldType.Unknown : field.type;
  }

  /**
   * Return the resource type of the field the provided edit affects.
   *
   * @private
   * @throws {BuilderError}
   *   If the edit resource type is unknown.
   * @param {Edit} unmarshalledEdit
   *   The unmarshalled edit.
   * @return {string}
   *   The resource type of the field of the provided edit.
   */
  #getResourceType(unmarshalledEdit: Edit): string {
    const fieldType: FieldType = this.#getFieldType(unmarshalledEdit);

    switch (fieldType) {
      case FieldType.SingleValueTaxonomy:
        return "paragraph--tbe_edit_taxonomy_single";
      case FieldType.MultipleValueTaxonomy:
        return "paragraph--tbe_edit_taxonomy_multi";
      default:
        throw new BuilderError(Drupal.t("Unknown edit resource type."));
    }
  }

  /**
   * @inheritdoc
   */
  buildRequestDataAttributes(): Attributes | null {
    if (!this.formValues.filter) return null;

    return {
      query: {
        value: this.formValues.filter,
      },
    };
  }

  /**
   * @inheritdoc
   */
  buildRequestDataRelationships(): Relationships | null {
    if (!this.#savableEdits.length) return null;

    return {
      edits: {
        data: this.#editUUIDs.map(
          (uuid: UUID, index: number): ResourceObject => ({
            id: uuid,
            type: this.#getResourceType(this.#savableEdits[index]),
          })
        ),
      },
    };
  }

  /**
   * @inheritdoc
   */
  buildRequestData(
    requestDataAttributes: Attributes | null,
    requestDataRelationships: Relationships | null
  ): NewBulkEditResourceObject {
    const requestData: NewBulkEditResourceObject = {
      type: this.transactionType,
    };

    if (requestDataAttributes !== null) {
      requestData.attributes = requestDataAttributes;
    }

    if (requestDataRelationships !== null) {
      requestData.relationships = requestDataRelationships;
    }

    return requestData;
  }

  /**
   * Return one or more relationships that should be included in this request
   * to provide data for the provided edit.
   *
   * @private
   * @throws {BuilderError}
   *   If the edit field type is unknown.
   * @param {Edit} unmarshalledEdit
   *   An unmarshalled edit.
   * @return {Relationships}
   *   One or more relationships that should be included in this request to
   *   provide data for the provided edit.
   */
  #buildIncludedElementRelationships(unmarshalledEdit: Edit): Relationships {
    const fieldType: FieldType = this.#getFieldType(unmarshalledEdit);
    const { resourceType }: Field = this.fields[unmarshalledEdit.field];

    switch (fieldType) {
      case FieldType.SingleValueTaxonomy: {
        const relationshipsBuilder: SingleValueTaxonomyRelationshipsBuilder =
          new SingleValueTaxonomyRelationshipsBuilder(
            unmarshalledEdit as Edit.Taxonomy.SingleValue,
            resourceType
          );
        return relationshipsBuilder.buildRelationships();
      }
      case FieldType.MultipleValueTaxonomy: {
        const relationshipsBuilder: MultipleValuesTaxonomyRelationshipsBuilder =
          new MultipleValuesTaxonomyRelationshipsBuilder(
            unmarshalledEdit as Edit.Taxonomy.MultipleValues,
            resourceType
          );
        return relationshipsBuilder.buildRelationships();
      }
      default:
        throw new BuilderError(Drupal.t("Unknown edit field type."));
    }
  }

  /**
   * Reducer function that can be passed to `Array.prototype.reduce()` to
   * marshall a single unmarshalled edit and return an array of accumulated
   * marshalled edits.
   *
   * See MDN's {@link
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce#parameters
   * `Array.prototype.reduce()`} documentation for more information.
   *
   * @private
   * @param {ResourceObject[]} acc
   *   The value resulting from the previous call to this function.
   * @param {Edit} unmarshalledEdit
   *   The current unmarshalled edit.
   * @param {number} index
   *   The index of the current unmarshalled edit.
   * @return {ResourceObject[]}
   *   An array of marshalled edits.
   */
  #includedReducer(
    acc: ResourceObject[],
    unmarshalledEdit: Edit,
    index: number
  ): ResourceObject[] {
    try {
      const element: ResourceObject = {
        id: this.#editUUIDs[index],
        type: this.#getResourceType(unmarshalledEdit),
        attributes: {
          machine_name: unmarshalledEdit.field,
          mode: unmarshalledEdit.method,
        },
        relationships:
          this.#buildIncludedElementRelationships(unmarshalledEdit),
        meta: {
          persistence_behavior: "create_or_update",
        },
      };
      return [...acc, element];
    } catch (err) {
      // If an error is thrown while building this element, catch the error here
      // so that the element is not included in the array and attempts can
      // nonetheless be made to build other elements.
      return acc;
    }
  }

  /**
   * @inheritdoc
   */
  buildRequestIncluded(): ResourceObject[] | null {
    // Bind `this.#includedReducer` to this method's value of `this`. If we
    // didn't do this, it would be `undefined` in `this.#includedReducer`.
    //
    // https://stackoverflow.com/a/59060545
    const included: ResourceObject[] = this.#savableEdits.reduce(
      this.#includedReducer.bind(this),
      []
    );

    if (included.length) {
      return included;
    } else {
      return null;
    }
  }
}
