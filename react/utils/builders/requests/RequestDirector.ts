import type { Request, ResourceObject } from "ts-json-api";
import type {
  BulkEditResourceObject,
  NewBulkEditResourceObject,
} from "@custom-types/api";
import type RequestBuilder from "./RequestBuilder";

/**
 * Class representing a Director, as described in the Gang of Four Builder
 * pattern, which can be used to direct the building of requests.
 */
export default class RequestDirector<
  DataType extends BulkEditResourceObject | NewBulkEditResourceObject
> {
  #requestBuilder: RequestBuilder<DataType>;

  /**
   * Construct a new instance.
   *
   * @param {RequestBuilder<DataType>} requestBuilder
   *   The Builder, as described by the Gang of Four Builder pattern, that will
   *   be used to build the request.
   */
  constructor(requestBuilder: RequestBuilder<DataType>) {
    this.#requestBuilder = requestBuilder;
  }

  /**
   * Use the provided Builder to create a new request.
   *
   * @return {Request<DataType>}
   *   The request that was built by the Builder.
   */
  createRequest(): Request<DataType> {
    // The Gang of Four mention that returning values from Builder methods and
    // passing them around is usually not necessary, but doing so here helps us
    // to enforce type safety.

    const requestData: DataType = this.#requestBuilder.buildRequestData(
      this.#requestBuilder.buildRequestDataAttributes(),
      this.#requestBuilder.buildRequestDataRelationships()
    );

    const requestIncluded: ResourceObject[] | null =
      this.#requestBuilder.buildRequestIncluded();

    return this.#requestBuilder.buildRequest(requestData, requestIncluded);
  }
}
