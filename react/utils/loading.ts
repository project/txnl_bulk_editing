import retry from "retry";
import UnavailableAfterRetryError from "@utils/errors/UnavailableAfterRetryError";
import { FetchStatus } from "@custom-types/state";

import type { Fetchable } from "@custom-types/state";

/**
 * Return a `Promise` which resolves to the data of a `Fetchable`. If the
 * `Fetchable` has not finished loading when this function is first called,
 * several additional attempts are made to access the data. If the data cannot
 * be accessed after several attempts, the `Promise` rejects with an
 * `UnavailableAfterRetryError`.
 *
 * Note that this function does not initiate the fetch of the `Fetchable`. It
 * only returns a `Promise` which resolves to its data if it becomes available.
 *
 * @param {Function} accessor
 *   A function which can be called to access the data of a `Fetchable`.
 * @return {Promise<T>}
 *   A Promise which resolves to the data of the `Fetchable` or rejects with an
 *   `UnavailableAfterRetryError` if the data cannot be accessed.
 */
export async function getFetchableWithRetries<T>(
  accessor: () => Fetchable<T>
): Promise<T> {
  // The _retry_ API is pretty peculiar, in my opinion. See its documentation
  // for more information.
  //
  // https://www.npmjs.com/package/retry
  //
  // Among other things, notice that a value must be passed to
  // `retryOperation.retry` to trigger a retry. The value doesn't strictly need
  // to be an `Error`, but the documentation suggests that it should be.

  const operation = retry.operation();

  return new Promise<T>((resolve, reject) => {
    operation.attempt(() => {
      const { data, status } = accessor();

      if (status === FetchStatus.Fulfilled && data !== null) {
        resolve(data);
      } else if (!operation.retry(new UnavailableAfterRetryError())) {
        // We have run out of attempts. The operation will not be retried.
        reject(new UnavailableAfterRetryError(true));
      }
    });
  });
}
