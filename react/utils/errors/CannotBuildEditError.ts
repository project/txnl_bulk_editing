/**
 * Abstract class which provides an interface for errors that can be thrown
 * while building form values.
 *
 * @abstract
 */
export default abstract class CannotBuildEditError extends Error {
  /**
   * Construct a new instance.
   *
   * @param {string} type
   *   The human-readable type of the edit.
   * @param {string} method
   *   The human-readable method of the edit.
   */
  constructor(type: string, method: string) {
    super(
      Drupal.t("Cannot build @type edit with method @method.", {
        "@type": type,
        "@method": method,
      })
    );
    this.name = "CannotBuildEditError";
  }
}
