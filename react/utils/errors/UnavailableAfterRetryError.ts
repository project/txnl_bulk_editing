/**
 * Class representing an error that can be thrown if some resource is
 * unavailable after one or more attempts to retrieve it.
 *
 * This error can be helpful when using the _retry_ module.
 */
export default class UnavailableAfterRetryError extends Error {
  /**
   * Construct a new instance.
   *
   * @param {boolean} maxAttempts=false
   *   Whether the maximum number of retry attempts has been reached.
   */
  constructor(maxAttempts: boolean = false) {
    if (maxAttempts) {
      super(
        Drupal.t(
          "Resource is unavailable after the maximum number of attempts."
        )
      );
    } else {
      super(Drupal.t("Resource is unavailable"));
    }

    this.name = "UnavailableAfterRetryError";
  }
}
