/**
 * Class representing an error that can be thrown if it is not possible to
 * build something while following the Builder pattern described by the Gang of
 * Four.
 */
export default class BuilderError extends Error {
  /**
   * Construct a new instance.
   *
   * @param {string} message
   *   A human-readable description of the error.
   */
  constructor(message: string) {
    super(message);
    this.name = "BuilderError";
  }
}
