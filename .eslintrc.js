module.exports = {
  root: true,
  extends: ["@inveniem/eslint-config"],
  parserOptions: {
    sourceType: "module",
  },
  rules: {
    // This rule is configured identically in eslint-config-airbnb, except
    // for the following customizations:
    //
    // * `Form.Item` is added as an exception.
    //
    // Corresponding configuration in eslint-config-airbnb-base:
    // https://github.com/airbnb/javascript/blob/b4377fb03089dd7f08955242695860d47f9caab4/packages/eslint-config-airbnb/rules/react.js#L494-L501
    "react/jsx-props-no-spreading": [
      "error",
      {
        html: "enforce",
        custom: "enforce",
        explicitSpread: "ignore",
        exceptions: ["Form.Item"],
      },
    ],
  },
};
